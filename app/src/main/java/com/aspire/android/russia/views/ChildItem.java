package com.aspire.android.russia.views;

import com.aspire.android.russia.model.Restaurant;

/**
 * Created by ryanbrooks on 6/17/15.
 */
public class ChildItem {

    private Restaurant restaurant;

    public ChildItem(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setText(Restaurant child) {
        this.restaurant = child;
    }
}
