package com.aspire.android.russia.adapter;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.Branches;
import com.aspire.android.russia.model.ExperiencesDetailObject;
import com.aspire.android.russia.model.Menu;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.Fontfaces;
//import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by chau.nguyen on 10/26/2016.
 */
public class ExperiencesDetailAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM_VIEW_HEADER = 0;
    public static final int ITEM_VIEW_NORMAL = 1;
    public static final int ITEM_VIEW_COMMON = 2;

    private List<ExperiencesDetailObject> data;
    private final OnRecyclerViewItemClickListener listener;

    public enum ROW_SHOW {
        CUISINE,
        HOURS,
        DRESS_CODE,
        TERMS_CONDITION,
        LOCATION,
        LOCATION_BRANCH,
        TELEPHONE,
        WEBSITE,
        EMAIL,
        MENU
    }

    public ExperiencesDetailAdapter(List<ExperiencesDetailObject> data,
                                    OnRecyclerViewItemClickListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View view;
        switch (viewType) {
            case ITEM_VIEW_HEADER:
                view = LayoutInflater.from(parent.getContext())
                                     .inflate(R.layout.layout_item_experience_gourmet_header,
                                              parent,
                                              false);
                return new DetailViewHeaderHolder(view);
            case ITEM_VIEW_COMMON:
                view = LayoutInflater.from(parent.getContext())
                                     .inflate(R.layout.layout_item_experience_gourmet_common,
                                              parent,
                                              false);
                return new DetailCommonViewHolder(view);
            default:
                //Normal
                view = LayoutInflater.from(parent.getContext())
                                     .inflate(R.layout.layout_item_experience_gourmet,
                                              parent,
                                              false);
                return new DetailNormalViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,
                                 int position) {
        ExperiencesDetailObject item = data.get(position);

        switch (getItemViewType(position)) {
            case ITEM_VIEW_HEADER:
                ((DetailViewHeaderHolder) holder).bind(position,
                                                       item,
                                                       listener);
                break;
            case ITEM_VIEW_NORMAL:
                ((DetailNormalViewHolder) holder).bind(position,
                                                       item,
                                                       listener);
                break;
            case ITEM_VIEW_COMMON:
                ((DetailCommonViewHolder) holder).bind(position,
                                                       item,
                                                       listener);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (data == null) ?
               0 :
               data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position)
                   .getTypeRowView();
    }

    public void addHeaderText(ExperiencesDetailObject detailObject) {
        data.add(detailObject);
        notifyItemInserted(getItemCount() - 1);
    }

    public class DetailNormalViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDetailTitle)
        TextView tvTitle;
        @BindView(R.id.tvDetailContent)
        TextView tvContent;
        @BindView(R.id.imgDetail)
        ImageView imgIc;
        int pos;

        public DetailNormalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,
                             itemView);
            CommonUtils.setFontForTextView(tvTitle,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
            CommonUtils.setFontForTextView(tvContent,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        }

        protected void bind(int position,
                            final ExperiencesDetailObject item,
                            final OnRecyclerViewItemClickListener event) {
            this.pos = position;

            switch (item.getRowShow()) {
                case CUISINE:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_gourmet_cuisine));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_cuisine);
                    break;
                case HOURS:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_gourmet_hours));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_time_operation);
                    break;
                case DRESS_CODE:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_gourmet_dress));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_dress_code);
                    break;
                case TERMS_CONDITION:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_gourmet_term));
                    CommonUtils.convertHtmlShowHtml(item.getContent(),
                                                    tvContent);
                    imgIc.setImageResource(R.drawable.ic_term);
                    break;
                case TELEPHONE:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_telephone));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_phone_num);
                    break;
                case WEBSITE:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_website));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_website);
                    break;
                case EMAIL:
                    tvTitle.setText(itemView.getContext()
                                            .getString(R.string.experiences_detail_email));
                    tvContent.setText(item.getContent());
                    imgIc.setImageResource(R.drawable.ic_email);
                    break;
                default:
                    break;
            }

            itemView.setTag(item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    event.onItemClick(pos,
                                      itemView);
                }
            });
        }
    }

    public class DetailCommonViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDetailTitle)
        TextView tvContent1;
        @BindView(R.id.tvDetailContent)
        TextView tvContent2;
        @BindView(R.id.imgArrow)
        ImageView imgArrow;
        @BindView(R.id.tvDetailSubLine)
        TextView tvContent3;

        @BindView(R.id.imgFood)
        ImageView imgIcon;

        int pos;

        public DetailCommonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,
                             itemView);
            CommonUtils.setFontForViewRecursive(tvContent1,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            CommonUtils.setFontForViewRecursive(tvContent2,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            CommonUtils.setFontForViewRecursive(tvContent3,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        }

        protected void bind(int position,
                            final ExperiencesDetailObject item,
                            final OnRecyclerViewItemClickListener event) {
            this.pos = position;

            switch (item.getRowShow()) {
                case LOCATION:
                    setTvContent1(item.getContent());
                    setTvContent2(item.getContent2());
                    tvContent3.setVisibility(View.GONE);
                    break;
                case LOCATION_BRANCH:
                    if (item.getBranches() != null) {
                        Branches branches = item.getBranches();
                        setTvContent1(branches.getAddress1());
                        setTvContent2(branches.getAddress2());
                        setTvContent3(branches.getZipcode());
                    }
                    break;
                case MENU:
                    imgArrow.setVisibility(View.GONE);
                    tvContent1.setTypeface(Fontfaces.getFont(itemView.getContext(),
                                                             Fontfaces.FONTLIST.Avenirnext_demibold));
                    tvContent2.setTextColor(ContextCompat.getColor(itemView.getContext(),
                                                                   R.color.experiences_item_menu));
                    tvContent3.setTypeface(Fontfaces.getFont(itemView.getContext(),
                                                             Fontfaces.FONTLIST.Avenirnext_demibold));
                    tvContent3.setTextColor(ContextCompat.getColor(itemView.getContext(),
                                                                   R.color.app_color_dark_bg));
                    if (item.getMenu() != null) {
                        Menu menu = item.getMenu();
                        setTvContent1(menu.getName());
                        setTvContent2(menu.getDescription());
                        setTvContent3(menu.getPrice());
                        setImageFood(menu.getImageUrl());
                    }
                    break;
                default:
                    break;
            }

            itemView.setTag(item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    event.onItemClick(pos,
                                      itemView);
                }
            });
        }


        private void setTvContent1(String data) {
            if (!data.isEmpty()) {
                tvContent1.setText(data);
                tvContent1.setVisibility(View.VISIBLE);
            } else {
                tvContent1.setVisibility(View.GONE);
            }
        }

        private void setTvContent2(String data) {
            if (!data.isEmpty()) {
                tvContent2.setText(data);
                tvContent2.setVisibility(View.VISIBLE);
            } else {
                tvContent2.setVisibility(View.GONE);
            }
        }

        private void setTvContent3(String data) {
            if (!data.isEmpty()) {
                tvContent3.setText(data);
                tvContent3.setVisibility(View.VISIBLE);
            } else {
                tvContent3.setVisibility(View.GONE);
            }
        }

        private void setImageFood(String url) {
            if (url.isEmpty()) {
                imgIcon.setVisibility(View.GONE);
            } else {
                imgIcon.setVisibility(View.VISIBLE);
//                Glide.with(itemView.getContext())
//                        .load(BuildConfig.WS_ROOT_URL + url)
//                        .centerCrop()
//                        .crossFade()
//                        .into(imgIcon);
            }
        }
    }

    public class DetailViewHeaderHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDetailHeader)
        TextView tvTitleHeader;
        int pos;

        public DetailViewHeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,
                             itemView);
        }

        protected void bind(int position,
                            final ExperiencesDetailObject item,
                            final OnRecyclerViewItemClickListener event) {
            this.pos = position;
            tvTitleHeader.setText(item.getContent());
            switch (item.getRowShow()) {
                case LOCATION_BRANCH:
                    tvTitleHeader.setTextColor(ContextCompat.getColor(itemView.getContext(),
                                                                      R.color.experiences_item_text_sub));
                    tvTitleHeader.setTypeface(Fontfaces.getFont(itemView.getContext(),
                                                                Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
                    break;
                default:
                    tvTitleHeader.setTypeface(Fontfaces.getFont(itemView.getContext(),
                                                                Fontfaces.FONTLIST.Avenirnext_demibold));
                    tvTitleHeader.setTextColor(ContextCompat.getColor(itemView.getContext(),
                                                                      R.color.app_color_dark_bg));
                    break;
            }
        }
    }

}
