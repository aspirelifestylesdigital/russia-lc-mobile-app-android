package com.aspire.android.russia.apiservices.b2c.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;


public class B2CLoginResponse
        extends B2CBaseResponse {
    @Expose
    private boolean HasForgotPassword;
    @Expose
    private String OnlineMemberID;
    @Expose
    private String VeriCode;

    public boolean isHasForgotPassword() {
        return HasForgotPassword;
    }

    public void setHasForgotPassword(boolean hasForgotPassword) {
        HasForgotPassword = hasForgotPassword;
    }

    public String getOnlineMemberID() {
        return OnlineMemberID;
    }

    public void setOnlineMemberID(String onlineMemberID) {
        OnlineMemberID = onlineMemberID;
    }

    public String getVeriCode() {
        return VeriCode;
    }

    public void setVeriCode(String veriCode) {
        this.VeriCode = veriCode;
    }
}
