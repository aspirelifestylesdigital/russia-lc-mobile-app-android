package com.aspire.android.russia.processing.cancelrequest;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.application.coreactivitys.AbstractActivity;
import com.aspire.android.russia.model.concierge.MyRequestObject;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public abstract class BaseCancelRequestDetail implements Callback,
                                                         B2CICallback {
    public interface ICancelRequestActionListener {
        void onCancelRequestClick(B2CUpsertConciergeRequestRequest b2CUpsertConciergeRequestRequest);
        void onCallConciergeClick(MyRequestObject myRequestObject);
    }

    protected View rootView;
    @BindView(R.id.tvRequestType)
    TextView tvRequestType;
    @BindView(R.id.tvRequestName)
    TextView tvRequestName;

    @BindView(R.id.tvCaseIdTitle)
    TextView tvCaseIdTitle;
    @BindView(R.id.tvCaseId)
    TextView tvCaseId;

    @BindView(R.id.tvDateOfRequestTitle)
    TextView tvDateOfRequestTitle;
    @BindView(R.id.tvRequestedDate)
    TextView tvRequestedDate;

    @BindView(R.id.tvRequestStatusTitle)
    TextView tvRequestStatusTitle;
    @BindView(R.id.tvRequestStatus)
    TextView tvRequestStatus;

    @BindView(R.id.tvTimeOfRequestTitle)
    TextView tvTimeOfRequestTitle;
    @BindView(R.id.tvRequestedTime)
    TextView tvRequestedTime;

    @BindView(R.id.rlRequestDetailHeader)
    View rlRequestDetailHeader;
    @BindView(R.id.tvRequestDetailHeader)
    TextView tvRequestDetailHeader;
    @BindView(R.id.ivRequestHeaderIndicator)
    ImageView ivRequestHeaderIndicator;

    @BindView(R.id.llRequestDetail)
    LinearLayout llRequestDetail;

    @BindView(R.id.tvCancelRequest)
    TextView tvCancelRequest;

    @BindView(R.id.rlCallConcierge)
    View rlCallConcierge;
    @BindView(R.id.tvCallConcierge)
    TextView tvCallConcierge;

    protected MyRequestObject myRequestObject;
    protected ICancelRequestActionListener cancelRequestActionListener;
    protected AbstractActivity activity;

    public BaseCancelRequestDetail(){}
    public BaseCancelRequestDetail(View view){
        rootView = view;
        ButterKnife.bind(this, view);

        activity = (AbstractActivity)view.getContext();

        // Set onclick listener
        tvCancelRequest.setOnClickListener(onClickListener);
        rlCallConcierge.setOnClickListener(onClickListener);
        rlRequestDetailHeader.setOnClickListener(onClickListener);

        // Set font
        // Demi-bold
        CommonUtils.setFontForViewList(FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD,
                tvRequestType, tvRequestName, tvCaseIdTitle, tvDateOfRequestTitle, tvTimeOfRequestTitle, tvRequestStatusTitle, tvCancelRequest, tvCallConcierge,tvRequestDetailHeader);
        // Medium
        CommonUtils.setFontForViewList(FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM, tvCaseId, tvRequestedDate, tvRequestedTime, tvRequestStatus);

    }
    public void setCancelRequestActionListener(ICancelRequestActionListener cancelRequestActionListener){
        this.cancelRequestActionListener = cancelRequestActionListener;
    }
    public void setMyRequestObject(MyRequestObject myRequestObject){
        this.myRequestObject = myRequestObject;
    }
    protected void updateUI(){
        updateCommonFields();
        rootView.setVisibility(View.VISIBLE);
    }
    protected void updateCommonFields(){
        tvRequestStatus.setText(myRequestObject.getRequestStatus()
                               );
    }
    protected void addRequestDetailItem(String title, String content){
        View itemView = LayoutInflater.from(rootView.getContext()).inflate(R.layout.ctrl_cancel_request_item_detail, null);
        TextView tvTitle = (TextView) itemView.findViewById(R.id.tvItemTitle);
        TextView tvContent = (TextView) itemView.findViewById(R.id.tvItemValue);
        CommonUtils.setFontForViewList(FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM, tvTitle, tvContent);

        // Set value
        tvTitle.setText(title);
        tvContent.setText(content);

        llRequestDetail.addView(itemView);
    }
    protected void showProgressDialog(){
        activity.showDialogProgress();
    }
    protected void hideProgressDialog(){
        activity.hideDialogProgress();
    }


    public abstract void getRequestDetail();
    public abstract B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest();
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.tvCancelRequest:
                    if(cancelRequestActionListener != null){
                        cancelRequestActionListener.onCancelRequestClick(getB2CUpsertConciergeRequestRequest());
                    }
                    break;
                case R.id.rlCallConcierge:
                    if(cancelRequestActionListener != null){
                        cancelRequestActionListener.onCallConciergeClick(myRequestObject);
                    }
                    break;
                case R.id.rlRequestDetailHeader:
                    /*if(llRequestDetail.getVisibility() == View.VISIBLE){
                        llRequestDetail.setVisibility(View.GONE);
                        ivRequestHeaderIndicator.setImageResource(R.drawable.down);
                    }else{
                        llRequestDetail.setVisibility(View.VISIBLE);
                        ivRequestHeaderIndicator.setImageResource(R.drawable.up);
                    }*/
                    break;
            }
        }
    };
}
