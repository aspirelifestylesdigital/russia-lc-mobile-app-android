package com.aspire.android.russia.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.fragment.BookEventFragment;
import com.aspire.android.russia.fragment.BookEventRecommendationFragment;
import com.aspire.android.russia.model.concierge.MyRequestObject;

/**
 * Created by anh.trinh on 9/15/2016.
 */
public class TabBookingEventAdapter
        extends FragmentPagerAdapter {
    private Context mContext;
    BaseFragment[] fragments = new BaseFragment[2];
    public TabBookingEventAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        mContext = context;
        BookEventFragment bookEventFragment = new BookEventFragment();
        BookEventRecommendationFragment bookEventRecomendFragment = new BookEventRecommendationFragment();
        if (bundle != null) {
            MyRequestObject myRequestObject = bundle.getParcelable(AppConstant.PRE_REQUEST_OBJECT_DATA);

            if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.HOTEL_REQUEST)) {

                bookEventFragment.setArguments(bundle);
            }else{
                if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.HOTEL_RECOMMEND_REQUEST)) {
                    bookEventRecomendFragment.setArguments(bundle);
                }
            }
        }

        fragments[0] = bookEventFragment;
        fragments[1] = bookEventRecomendFragment;
    }
    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }
    @Override
    public int getCount() {
        return fragments.length;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    public void makeDefaultDateTime(int position){
        if(position == 0){
            ((BookEventFragment)fragments[0]).makeDefaultDateTime();
        }else{
            ((BookEventRecommendationFragment)fragments[1]).makeDefaultDateTime();
        }
    }

}
