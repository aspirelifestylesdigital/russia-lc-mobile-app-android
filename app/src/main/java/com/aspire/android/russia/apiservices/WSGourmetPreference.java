package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.apiservices.RequestModel.UpdategGourmetRequest;
import com.aspire.android.russia.apiservices.ResponseModel.UpdateGourmetResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGourmetPreference
        extends ApiProviderClient<UpdateGourmetResponse> {

    public WSGourmetPreference(){

    }

    @Override
    public void run(Callback<UpdateGourmetResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        UpdategGourmetRequest request = new UpdategGourmetRequest();
        serviceInterface.updateGourmetPreferences(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
