package com.aspire.android.russia.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.aspire.android.russia.R;
import com.aspire.android.russia.adapter.SelectDataAdapter;
import com.aspire.android.russia.apiservices.ResponseModel.CuisineGourmetResponse;
import com.aspire.android.russia.apiservices.ResponseModel.CuisineTypeResponse;
import com.aspire.android.russia.apiservices.ResponseModel.OccasionResponse;
import com.aspire.android.russia.apiservices.ResponseModel.PreferrelTeeTimeResponse;
import com.aspire.android.russia.apiservices.WSGetCuisineGourmet;
import com.aspire.android.russia.apiservices.WSGetOccasion;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.KeyValueObject;
import com.aspire.android.russia.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class SelectDataFragment
        extends BaseFragment implements retrofit2.Callback,
        OnRecyclerViewItemClickListener{

    interface OnSelectDataListener {
        void onSelectedData(APIDATA type, List<KeyValueObject> list);
    }

    enum APIDATA {
        CUISINE_MY_PREFERENCES,
        PREFERRED_TEE_TIME,
        OCCASION,
        CUISINE_GOURMET
    }

    @BindView(R.id.rclView)
    RecyclerView rclView;

    private SelectDataAdapter adapter;
    private OnSelectDataListener onSelectListener;
    private String title;
    private APIDATA dataType = APIDATA.CUISINE_MY_PREFERENCES;

    public SelectDataFragment() {
        adapter = new SelectDataAdapter();
        adapter.setItemClickListener(this);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_cuisinetype;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this, view);

        //
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rclView.setLayoutManager(layoutManager);
        rclView.setAdapter(adapter);

        if(dataType== APIDATA.CUISINE_MY_PREFERENCES) {
            /*showDialogProgress();
            WSGetCuisineType ws = new WSGetCuisineType();
            ws.run(this);*/
            adapter.addData(getPreferredCuisine());
            adapter.notifyDataSetChanged();
        }
        else if(dataType== APIDATA.PREFERRED_TEE_TIME){
            /*showDialogProgress();
            WSGetPreferredTeeTimes ws = new WSGetPreferredTeeTimes();
            ws.run(this);*/
            adapter.addData(getPreferredTeeTimes());
            adapter.notifyDataSetChanged();
        }
        else if(dataType== APIDATA.OCCASION){
            showDialogProgress();
            WSGetOccasion ws = new WSGetOccasion();
            ws.run(this);
        }
        else if(dataType== APIDATA.CUISINE_GOURMET){
            showDialogProgress();
            WSGetCuisineGourmet ws = new WSGetCuisineGourmet();
            ws.run(this);
        }

    }
    private List<KeyValueObject> getPreferredCuisine(){
        List<KeyValueObject> cuisineList = new ArrayList<>();
        String[] cuisineResArr = getResources().getStringArray(R.array.cuisine_array);
        for(String cuisine : cuisineResArr){
            cuisineList.add(new KeyValueObject(cuisine, cuisine));
        }
        return cuisineList;
    }
    private List<KeyValueObject> getPreferredTeeTimes(){
        List<KeyValueObject> teeTimeList = new ArrayList<>();
        String[] teeTimeResArr = getResources().getStringArray(R.array.tee_time);
        for(String teeTime : teeTimeResArr){
            teeTimeList.add(new KeyValueObject(teeTime, teeTime));
        }
        return teeTimeList;
    }
    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        showLogoApp(false);
        setTitle(getString(R.string.text_title_preference));
        if(!TextUtils.isEmpty(title)){
            setTitle(title);
        }
        hideToolbarMenuIcon();

        showRightText(null, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showToast("select");
                if(onSelectListener!=null){
                    List<KeyValueObject>list = adapter.getCuisinesSelected();
                    onSelectListener.onSelectedData(dataType, list);
                }
                onBackPress();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideRightText();
        showToolbarMenuIcon();
    }

    @Override
    public void onResponse(Call call, retrofit2.Response response) {
        if(getActivity()==null) {
            return;
        }
        if(response.body() instanceof CuisineTypeResponse){
            CuisineTypeResponse rsp = (CuisineTypeResponse) response.body();
            if(rsp.isSuccess()){
                adapter.addData(rsp.getData());
                adapter.notifyDataSetChanged();
            }
            else{

            }
        }
        else if(response.body() instanceof PreferrelTeeTimeResponse){
            PreferrelTeeTimeResponse rsp = (PreferrelTeeTimeResponse) response.body();
            if(rsp.isSuccess()){
                adapter.addData(rsp.getData());
                adapter.notifyDataSetChanged();
            }
            else{

            }
        }
        else if(response.body() instanceof OccasionResponse){
            OccasionResponse rsp = (OccasionResponse) response.body();
            if(rsp.isSuccess()){
                adapter.addData(rsp.getData());
                adapter.notifyDataSetChanged();
            }
        }
        else if(response.body() instanceof CuisineGourmetResponse){
            CuisineGourmetResponse rsp = (CuisineGourmetResponse) response.body();
            if(rsp.isSuccess()){
                adapter.addData(rsp.getData());
                adapter.notifyDataSetChanged();
            }
        }

        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Logger.sout("cuisine type fail");
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
    }

    public void setOnSelectListener(OnSelectDataListener onSelectListener) {
        this.onSelectListener = onSelectListener;
    }

    public void setCuisineSelected(List<KeyValueObject>list){
        adapter.setCuisineSelected(list);
    }

    public void setDataType(APIDATA type){
        this.dataType = type;
    }
    public void setHeaderTitle(String title){this.title = title;}
    public void setChoiceMode(SelectDataAdapter.CHOICEMODE choiceMode) {
        adapter.setChoiceMode(choiceMode);
    }

    @Override
    public void onItemClick(int position, View view) {
        if(adapter.getChoiceMode()== SelectDataAdapter.CHOICEMODE.SINGLE){
            if(onSelectListener!=null){
                List<KeyValueObject>list = adapter.getCuisinesSelected();
                onSelectListener.onSelectedData(dataType, list);
            }
            onBackPress();
        }
    }

}