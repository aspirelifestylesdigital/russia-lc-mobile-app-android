package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.BookingRestaunrantData;


public class BookRestaurantResponse
        extends BaseResponse {
    private BookingRestaunrantData Data;

    public BookingRestaunrantData getData() {
        return Data;
    }

    public void setData(BookingRestaunrantData data) {
        this.Data = data;
    }
}
