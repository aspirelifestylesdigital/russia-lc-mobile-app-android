package com.aspire.android.russia.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.aspire.android.russia.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadingViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progressBar)
    public ProgressBar progressBar;

    public LoadingViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}