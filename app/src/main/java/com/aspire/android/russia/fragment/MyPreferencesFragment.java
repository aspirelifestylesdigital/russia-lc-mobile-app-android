package com.aspire.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.apiservices.ResponseModel.GetMyPreferenceResponse;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.CommonObject;
import com.aspire.android.russia.model.LoyalProgramObject;
import com.aspire.android.russia.model.MyPreferencesObject;
import com.aspire.android.russia.model.RentalObject;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.EntranceLock;
import com.aspire.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 9/27/2016.
 */
public class MyPreferencesFragment
        extends BaseFragment
        implements Callback {

    @BindView(R.id.tvMessage)
    protected TextView tvMessage;
    @BindView(R.id.tvCarRental)
    protected TextView tvCarRental;
    @BindView(R.id.tvGourmet)
    protected TextView tvGourmet;
    @BindView(R.id.tvGolf)
    protected TextView tvGolf;
    @BindView(R.id.tvHotel)
    protected TextView tvHotel;
    @BindView(R.id.tvOther)
    protected TextView tvOther;

    ArrayList<RentalObject> mVehicleSelected = new ArrayList<>();
    ArrayList<RentalObject> mCompanySelected = new ArrayList<>();
    String mOtherRentalCompany = "";

    MyPreferencesObject myPreferencesObject;
    LoyalProgramObject mLoyalProgramCarRental;
    LoyalProgramObject mLoyalProgramHotel;

    EntranceLock entranceLock = new EntranceLock();
    @Override
    protected int layoutId() {
        return R.layout.fragment_mypreference;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this,
                         view);
        /*String user = UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName();
        String message = getString(R.string.text_preference_message,
                                   user);
        tvMessage.setText(message);*/
        CommonUtils.setFontForViewRecursive(tvMessage,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvCarRental,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvGourmet,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvGolf,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvHotel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvOther,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

    }

    @Override
    protected void bindData() {
        //String mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE, "");
        /*!mUserID.equals("")*/

        /*if (UserItem.isLogined()) {
            showDialogProgress();
            WSGetMyPreferences wsGetMyPreferences = new WSGetMyPreferences();
            wsGetMyPreferences.getMyPreferences(UserItem.getLoginedId());
            wsGetMyPreferences.run(this);
        }*/
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof HomeActivity) {
            showLogoApp(false);
            setTitle(getString(R.string.title_mypreference));
        }

    }

    @OnClick({R.id.tvCarRental,
              R.id.tvGourmet,
              R.id.tvGolf,
              R.id.tvHotel,
              R.id.tvOther})
    public void onClick(View v) {
        if(!entranceLock.isClickContinuous()) {
        switch (v.getId()) {
            case R.id.tvCarRental:
                //showToast("Car");
                Bundle bundle = new Bundle();
                if (mOtherRentalCompany != null) {
                    bundle.putString(CarRentalTransfresFragment.PRE_OTHER_RENTAL_COMPANY_DATA,
                                     mOtherRentalCompany);
                }
                if (mVehicleSelected != null) {
                    bundle.putParcelableArrayList(CarRentalTransfresFragment.PRE_VEHICLE_DATA,
                                                  mVehicleSelected);
                }
                if (mCompanySelected != null) {
                    bundle.putParcelableArrayList(CarRentalTransfresFragment.PRE_RENTAL_COMPANY_DATA,
                                                  mCompanySelected);
                }
                if (mLoyalProgramCarRental != null) {
                    bundle.putParcelable(CarRentalTransfresFragment.PRE_LOYAL_PROGRAM_CAR_RENTAL_DATA,
                                         mLoyalProgramCarRental);
                }
                CarRentalTransfresFragment fragment = new CarRentalTransfresFragment();
                fragment.setArguments(bundle);
                pushFragment(fragment,
                             true,
                             true);
                break;
            case R.id.tvGourmet:
                //showToast("Gourmet");
                MyPreferencesGourmetFragment gourmet = new MyPreferencesGourmetFragment();
                if (myPreferencesObject != null) {
                    gourmet.onSelectedData(SelectDataFragment.APIDATA.CUISINE_MY_PREFERENCES, myPreferencesObject.getCuisinePreferences());
                }
                pushFragment(gourmet,
                             true,
                             true);
                break;
            case R.id.tvGolf:
                //showToast("Golf");
                MyPreferencesGolfFragment golf = new MyPreferencesGolfFragment();
                if (myPreferencesObject != null) {
                    golf.onSelectedData(SelectDataFragment.APIDATA.PREFERRED_TEE_TIME, myPreferencesObject.getTeeTimes());
                }
                pushFragment(golf,
                             true,
                             true);
                break;
            case R.id.tvHotel:

                HotelMyPreferencesFragment hotelMyPreferencesFragment =
                        new HotelMyPreferencesFragment();

                Bundle bundleHotel = new Bundle();
                if (myPreferencesObject != null) {
                    bundleHotel.putParcelable(HotelMyPreferencesFragment.PRE_RATING_DATA,
                                              myPreferencesObject.getRatingForApp());
                    ArrayList<CommonObject> roomType = new ArrayList<>();
                    roomType.addAll(myPreferencesObject.getRoomTypes());
                    bundleHotel.putParcelableArrayList(HotelMyPreferencesFragment.PRE_ROOM_TYPE_DATA,
                                                       roomType);
                    ArrayList<CommonObject> bedType = new ArrayList<>();
                    bedType.addAll(myPreferencesObject.getBeds());
                    bundleHotel.putParcelableArrayList(HotelMyPreferencesFragment.PRE_BED_DATA,
                                                       bedType);
                    bundleHotel.putParcelable(HotelMyPreferencesFragment.PRE_SMOKING_ROOM_DATA,
                                              myPreferencesObject.getSmookingRoom());
                    bundleHotel.putParcelable(HotelMyPreferencesFragment.PRE_LOYAL_PROGRAM_HOTEL_DATA,
                                              mLoyalProgramHotel);
                }
                hotelMyPreferencesFragment.setArguments(bundleHotel);
                pushFragment(hotelMyPreferencesFragment,
                             true,
                             true);

                break;
            case R.id.tvOther:
                //showToast("Other");
                MyPreferencesOtherFragment other = new MyPreferencesOtherFragment();
                if (myPreferencesObject != null) {
                    other.setOther(myPreferencesObject.getOthers());
                }
                pushFragment(other,
                             true,
                             true);
                break;
        }
    }
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if(getActivity()==null) {
            return;
        }
        if (response.body() instanceof GetMyPreferenceResponse) {
            GetMyPreferenceResponse myPreferenceResponse =
                    ((GetMyPreferenceResponse) response.body());
            //int code = myPreferenceResponse.getStatus();
            if (myPreferenceResponse.isSuccess()) {
                myPreferencesObject = myPreferenceResponse.getData();
                if (myPreferencesObject != null) {
                    if (mVehicleSelected == null) {
                        mVehicleSelected = new ArrayList<>();
                    }
                    mVehicleSelected.clear();
                    mVehicleSelected.addAll(myPreferencesObject.getRentalVehicleTypes());
                    if (mCompanySelected == null) {
                        mCompanySelected = new ArrayList<>();
                    }
                    mCompanySelected.clear();
                    mCompanySelected.addAll(myPreferencesObject.getRentalCompanies());

                    mOtherRentalCompany = myPreferencesObject.getCarRentalsandTransfersOthers();
                    getLoyalProgramCarRental(myPreferencesObject.getLoyalProgrammes());
                    getLoyalProgramHotel(myPreferencesObject.getLoyalProgrammes());
                }
            } else {
                String msg = myPreferenceResponse.getMessage();
                if(msg==null){
                    if(msg.equalsIgnoreCase(getString(R.string.text_my_preferences_record_not_found))){

                    }
                    else{
//                        showToast(msg);
                    }
                }
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        if(getActivity()==null) {
            return;
        }
        showInternetProblemDialog();

        hideDialogProgress();
//        showToast(getString(R.string.text_server_error_message));

    }

    private void getLoyalProgramCarRental(List<LoyalProgramObject> loyalProgramObjects) {

        for (LoyalProgramObject object : loyalProgramObjects) {
            if (object.getTempCategory()
                      .equals(LoyalProgramObject.PRE_TEMP_CATEGORY_CAR_RENTAL) && mLoyalProgramCarRental == null) {
                mLoyalProgramCarRental = object;
                return;
            }
        }
    }

    private void getLoyalProgramHotel(List<LoyalProgramObject> loyalProgramObjects) {

        for (LoyalProgramObject object : loyalProgramObjects
                ) {
            if (object.getTempCategory()
                      .equals(LoyalProgramObject.PRE_TEMP_CATEGORY_HOTEL) && mLoyalProgramHotel == null) {
                mLoyalProgramHotel = object;
                return;
            }

        }
    }

}
