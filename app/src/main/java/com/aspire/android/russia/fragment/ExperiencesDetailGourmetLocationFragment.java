package com.aspire.android.russia.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.aspire.android.russia.R;
import com.aspire.android.russia.adapter.ExperiencesDetailAdapter;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.Branches;
import com.aspire.android.russia.model.ExperiencesDetailObject;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.aspire.android.russia.adapter.ExperiencesDetailAdapter.ROW_SHOW.LOCATION;
import static com.aspire.android.russia.adapter.ExperiencesDetailAdapter.ROW_SHOW.LOCATION_BRANCH;

/*
 * Created by chau.nguyen on 10/20/2016.
 */
public class ExperiencesDetailGourmetLocationFragment extends BaseFragment {

    @BindView(R.id.rcvDetailCommon)
    RecyclerView rcv;
    List<ExperiencesDetailObject> listExpDetailObj;

    private Restaurant restaurant;

    public ExperiencesDetailGourmetLocationFragment() {
        listExpDetailObj = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail_gourmet_common;
    }

    @Override
    protected void initView() {

        if (restaurant != null) {

            if (!restaurant.getAddress().isEmpty()) {
                //add Header Location
                ExperiencesDetailObject objHeader = new ExperiencesDetailObject(getString(R.string.experiences_detail_gourmet_location));
                objHeader.setRowShow(LOCATION);
                listExpDetailObj.add(objHeader);

                ExperiencesDetailObject obj1 = new ExperiencesDetailObject(ExperiencesDetailAdapter.ROW_SHOW.LOCATION);
                obj1.setTypeRowView(ExperiencesDetailAdapter.ITEM_VIEW_COMMON);
                obj1.setContent(restaurant.getAddress());
                obj1.setContent2(restaurant.getCountryName() + " " + restaurant.getZipcode());
                listExpDetailObj.add(obj1);
            }

            if (restaurant.getBranches() != null && restaurant.getBranches().size() > 0) {
                //add Header Branches
                ExperiencesDetailObject objBranch = new ExperiencesDetailObject(getString(R.string.experiences_detail_gourmet_location_branch));
                objBranch.setRowShow(LOCATION_BRANCH);
                listExpDetailObj.add(objBranch);

                for (Branches branch : restaurant.getBranches()) {
                    ExperiencesDetailObject obj = new ExperiencesDetailObject(ExperiencesDetailAdapter.ROW_SHOW.LOCATION_BRANCH);
                    obj.setTypeRowView(ExperiencesDetailAdapter.ITEM_VIEW_COMMON);
                    obj.setBranches(branch);
                    listExpDetailObj.add(obj);
                }
            }

            ExperiencesDetailAdapter adapter = new ExperiencesDetailAdapter(listExpDetailObj, new OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(int position, View view) {
                    if (view.getTag() instanceof ExperiencesDetailObject) {

                        ExperiencesDetailObject obj = (ExperiencesDetailObject) view.getTag();

                        if (obj.getRowShow() == ExperiencesDetailAdapter.ROW_SHOW.LOCATION) {
                            openMap(restaurant.getLat(), restaurant.getLong());

                        } else if (obj.getRowShow() == ExperiencesDetailAdapter.ROW_SHOW.LOCATION_BRANCH) {
                            openMap(obj.getBranches().getLatitude(), obj.getBranches().getLongitude());

                        }
                    }

                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rcv.setLayoutManager(layoutManager);
            rcv.setAdapter(adapter);
        }
    }

    private void openMap(String latitude, String longitude) {
        try {
            String label = "";
            String uriBegin = "geo:" + Double.parseDouble(latitude) + "," + Double.parseDouble(longitude);
            String query = Double.parseDouble(latitude) + "," + Double.parseDouble(longitude) + "(" + label + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uriString));
            startActivity(intent);
        } catch (Exception e) {
            Logger.sout(getString(R.string.experiences_detail_error_map));
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}