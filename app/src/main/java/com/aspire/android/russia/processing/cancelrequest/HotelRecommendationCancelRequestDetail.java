package com.aspire.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.aspire.android.russia.R;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.model.concierge.HotelBookingDetailData;
import com.aspire.android.russia.utils.DateTimeUtil;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelRecommendationCancelRequestDetail extends BaseCancelRequestDetail{
    private HotelBookingDetailData hotelRecommendationBookingDetailData;
    public HotelRecommendationCancelRequestDetail() {
    }

    public HotelRecommendationCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetHotelRecommendationBookingDetail wsGetHotelRecommendationBookingDetail = new WSGetHotelRecommendationBookingDetail();
        wsGetHotelRecommendationBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetHotelRecommendationBookingDetail.run(this);
        showProgressDialog();*/
    }
    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = hotelRecommendationBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_hotel_recommend_request));
        // Request name
            tvRequestName.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_recommend_a_hotel) + hotelRecommendationBookingDetailData.getCity());

        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_hotel_recommend_request_detail));

        if(hotelRecommendationBookingDetailData != null){
            // Case Id
            tvCaseId.setText(hotelRecommendationBookingDetailData.getBookingItemID());

            // Guests
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_guests), hotelRecommendationBookingDetailData.getGuestDisplay());

            // Room and Suites
            String roomSuitesObj = hotelRecommendationBookingDetailData.getRoomType();

            if(roomSuitesObj != null && !TextUtils.isEmpty(roomSuitesObj)){
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_room_preference), roomSuitesObj);
            }

            // Check In Date
            long checkInDate = hotelRecommendationBookingDetailData.getCheckInDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_check_in_date), DateTimeUtil.shareInstance().formatTimeStamp(checkInDate, "dd MMMM yyyy"));

            // Check In Date
            long checkOutDate = hotelRecommendationBookingDetailData.getCheckOutDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_check_out_date), DateTimeUtil.shareInstance().formatTimeStamp(checkOutDate, "dd MMMM yyyy"));
        }
    }

    @Override
    public void onResponse(Call call, Response response) {
        /*hideProgressDialog();
        if(response != null && response.body() instanceof HotelRecommendationBookingDetailResponse){
            //hotelRecommendationBookingDetailData = ((HotelRecommendationBookingDetailResponse)response.body()).getData();
            updateUI();
        }*/
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if(response instanceof B2CGetRecentRequestResponse){
            hotelRecommendationBookingDetailData = new HotelBookingDetailData((B2CGetRecentRequestResponse)response);
            updateUI();
        }
    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {

    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.HOTEL_RECOMMEND.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_HOTEL.getValue());
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getCity())){
            upsertConciergeRequestRequest.setCity(hotelRecommendationBookingDetailData.getCity());
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getCountry())){
            upsertConciergeRequestRequest.setCountry(hotelRecommendationBookingDetailData.getCountry());
        }
        if(hotelRecommendationBookingDetailData.getNumberOfAdults() > 0){
            upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(hotelRecommendationBookingDetailData.getNumberOfAdults()));
        }
        if(hotelRecommendationBookingDetailData.getNumberOfKids() > 0){
            upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(hotelRecommendationBookingDetailData.getNumberOfKids()));
        }

        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                                                                                       "")));
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getMaxPrice())){
            upsertConciergeRequestRequest.setPhoneNumber(hotelRecommendationBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(hotelRecommendationBookingDetailData.getPrefResponse());
        // Check in date
        upsertConciergeRequestRequest.setStartDate(DateTimeUtil.shareInstance().formatTimeStamp(hotelRecommendationBookingDetailData.getCheckInDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(hotelRecommendationBookingDetailData.getCheckInDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckIn(DateTimeUtil.shareInstance().formatTimeStamp(hotelRecommendationBookingDetailData.getCheckInDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        // Check out date
        upsertConciergeRequestRequest.setEndDate(DateTimeUtil.shareInstance().formatTimeStamp(hotelRecommendationBookingDetailData.getCheckOutDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckOut(DateTimeUtil.shareInstance().formatTimeStamp(hotelRecommendationBookingDetailData.getCheckOutDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));


        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(hotelRecommendationBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getConciergeRequestDetail()
                                                                  .getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + hotelRecommendationBookingDetailData.getConciergeRequestDetail()
                                                                                                               .getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getMaxPrice())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + hotelRecommendationBookingDetailData.getMaxPrice();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_GUEST_NAME + hotelRecommendationBookingDetailData.getReservationName();
        }

        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getLoyaltyProgrammeMembership())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERSHIP + hotelRecommendationBookingDetailData.getLoyaltyProgrammeMembership();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getMemberNo())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERNO + hotelRecommendationBookingDetailData.getMemberNo();
        }
        if (!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + hotelRecommendationBookingDetailData.getCountry();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getRoomType())){
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_ROOM_PREF + hotelRecommendationBookingDetailData.getRoomType();
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_SMOKING_PREF + (hotelRecommendationBookingDetailData.getSmokingPreference() ? "Yes" : "No");
        return requestDetails;
       /* String requestDetails = "";
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getConciergeRequestDetail()
                                                    .getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + hotelRecommendationBookingDetailData.getConciergeRequestDetail()
                                                                                                               .getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getMaxPrice())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + hotelRecommendationBookingDetailData.getMaxPrice();
        }

        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getLoyaltyProgrammeMembership())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERSHIP + hotelRecommendationBookingDetailData.getLoyaltyProgrammeMembership();
        }
        if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getMemberNo())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERNO + hotelRecommendationBookingDetailData.getMemberNo();
        }
            if(!TextUtils.isEmpty(hotelRecommendationBookingDetailData.getRoomType())){
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails += AppConstant.B2C_REQUEST_DETAIL_ROOM_PREF + hotelRecommendationBookingDetailData.getRoomType();
            }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_SMOKING_PREF + (hotelRecommendationBookingDetailData.getSmokingPreference() ? "Yes" : "No");
        return requestDetails;*/
    }
}
