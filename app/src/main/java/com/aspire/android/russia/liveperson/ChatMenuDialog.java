package com.aspire.android.russia.liveperson;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.aspire.android.russia.R;
import com.liveperson.messaging.sdk.api.LivePerson;

import static com.aspire.android.russia.liveperson.ChatActivity.DIALOG_CLEAR_HISTORY;
import static com.aspire.android.russia.liveperson.ChatActivity.DIALOG_MARK_AS_NOT_URGENT;
import static com.aspire.android.russia.liveperson.ChatActivity.DIALOG_MARK_AS_RESOLVED;
import static com.aspire.android.russia.liveperson.ChatActivity.DIALOG_MARK_AS_URGENT;
import static com.aspire.android.russia.liveperson.ChatActivity.DIALOG_RESOLVE_CONVERSATION;

public class ChatMenuDialog extends android.support.v4.app.DialogFragment {

    public static ChatMenuDialog newInstance(int title, int message, int dialogID) {
        ChatMenuDialog fragment = new ChatMenuDialog();

        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("message", message);
        args.putInt("dialogId", dialogID);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        switch (getArguments().getInt("dialogId")) {
            case DIALOG_CLEAR_HISTORY:
                builder.setPositiveButton(R.string.text_clear, (dialogInterface, i) -> {
                    LivePerson.clearHistory();
                });
                break;
            case DIALOG_MARK_AS_RESOLVED:
                builder.setPositiveButton(R.string.text_resolve, (dialogInterface, i) -> {
                    LivePerson.resolveConversation();
                });
                break;
            case DIALOG_RESOLVE_CONVERSATION:
                builder.setPositiveButton(R.string.text_ok_ru, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });
                break;
   /*         case DIALOG_MARK_AS_URGENT:
                builder.setPositiveButton(R.string.text_ok, (dialogInterface, i) -> {
                    ((ChatActivity)getActivity()).markAsUrgent();
                });
                break;
            case DIALOG_MARK_AS_NOT_URGENT:
                builder.setPositiveButton(R.string.text_ok, (dialogInterface, i) -> {
                    ((ChatActivity)getActivity()).markAsNotUrgent();
                });
                break;*/
        }
        builder.setTitle(getArguments().getInt("title"));
        builder.setMessage(getArguments().getInt("message"));
        if (getArguments().getInt("dialogId") != DIALOG_RESOLVE_CONVERSATION) {
            builder.setNegativeButton(R.string.text_cancel, (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });
        }
        return builder.create();
    }
}
