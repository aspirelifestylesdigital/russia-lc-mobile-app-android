package com.aspire.android.russia.apiservices.b2c.RequestModel;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.RequestModel.BaseRequest;
import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CLoginRequest extends BaseRequest{
    @Expose
    private String ConsumerKey;
    @Expose
    private String Email2;
    @Expose
    private String Functionality;
    @Expose
    private String MemberDeviceId;
    @Expose
    private String Password;

    public B2CLoginRequest(){
        ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        Functionality = "Login";
        MemberDeviceId = BuildConfig.B2C_MEMBER_DEVICE_ID;
    }
    public B2CLoginRequest build(String email, String password){
        this.Email2 = email;
        this.Password = password;
        return this;
    }

}
