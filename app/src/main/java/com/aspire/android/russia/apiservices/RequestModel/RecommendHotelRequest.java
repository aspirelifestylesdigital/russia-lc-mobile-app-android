package com.aspire.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class RecommendHotelRequest
        extends BaseRequest {
    @Expose
    private String BookingId="";
    @Expose
    private String UserID="";
    @Expose
    private String UploadPhoto="";
    @Expose
    private Boolean Phone = false;
    @Expose
    private Boolean Email= false;
    @Expose
    private Boolean Both= false;
    @Expose
    private String MobileNumber="";
    @Expose
    private String EmailAddress = "";
    @Expose
    private String CheckInDate="";
    @Expose
    private String CheckOutDate="";
    @Expose
    private String RoomType="";
    @Expose
    private String StarRating="";
    @Expose
    private String MinimumPrice="";
    @Expose
    private String MaximumPrice="";
    @Expose
    private Integer NumberOfAdults=0;
    @Expose
    private String Country="";
    @Expose
    private String City="";
    @Expose
    private Integer NumberOfKids=0;
    @Expose
    private Boolean SmokingPreference= false;
    @Expose
    private String SpecialRequirements ="";


    public String getRating() {
        return StarRating;
    }

    public void setRating(final String rating) {
        StarRating = rating;
    }


    public Boolean getSmokingPreference() {
        return SmokingPreference;
    }

    public void setSmokingPreference(final Boolean smokingPreference) {
        SmokingPreference = smokingPreference;
    }


    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(final String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(final String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getRoomType() {
        return RoomType;
    }

    public void setRoomType(final String roomType) {
        RoomType = roomType;
    }




    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(final String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(final String maximumPrice) {
        MaximumPrice = maximumPrice;
    }



    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(final String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(final String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(final Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(final Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(final Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        EmailAddress = emailAddress;
    }



    public String getCountry() {
        return Country;
    }

    public void setCountry(final String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(final String city) {
        City = city;
    }

    public Integer getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(final Integer numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public Integer getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(final Integer numberOfKids) {
        NumberOfKids = numberOfKids;
    }




}
