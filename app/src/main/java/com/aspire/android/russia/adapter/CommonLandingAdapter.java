package com.aspire.android.russia.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.CommonLandingObject;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by chau.nguyen on 10/5/2016.
 */

public class CommonLandingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CommonLandingObject> data;
    private final OnRecyclerViewItemClickListener listener;

    public CommonLandingAdapter(List<CommonLandingObject> data, OnRecyclerViewItemClickListener listener) {
        if (this.data == null) {
            this.data = new ArrayList<>();
        }
        this.data.addAll(data);
        //event click
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_common_landing, parent, false);
        return new CommonLandingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CommonLandingObject item = data.get(position);
        ((CommonLandingViewHolder) holder).bind(position, item, listener);
    }

    @Override
    public int getItemCount() {
        if (data == null)
            return 0;
        return data.size();
    }

    public class CommonLandingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCommonLanding)
        TextView tvConcierge;
        @BindView(R.id.imgCommonLanding)
        ImageView imgConcierge;
        int pos;

        public CommonLandingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            CommonUtils.setFontForTextView(tvConcierge,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        }

        protected void bind(int position, final CommonLandingObject item, final OnRecyclerViewItemClickListener event) {
            this.pos = position;
            tvConcierge.setText(item.getTitle());
            imgConcierge.setImageResource(item.getImgId());
            itemView.setTag(item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    event.onItemClick(pos, itemView);
                }
            });
        }
    }

}
