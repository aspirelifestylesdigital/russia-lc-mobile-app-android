package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CGetUserDetailsRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetUserDetailsResponse;
import com.aspire.android.russia.model.UserItem;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSGetUserDetails
        extends B2CApiProviderClient<B2CGetUserDetailsResponse>{

    public B2CWSGetUserDetails(B2CICallback callback){
        this.b2CICallback = callback;
    }
    @Override
    public void run(Callback<B2CGetUserDetailsResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        serviceInterface.getUserDetails(new B2CGetUserDetailsRequest()).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CGetUserDetailsResponse response) {
        // Save user item to preference
        UserItem userItem = new UserItem(response);
        UserItem.savePreference(userItem);
    }
}
