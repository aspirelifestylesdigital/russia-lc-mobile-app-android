package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.UserObject;
import com.google.gson.annotations.Expose;


public class SignInResponse
        extends BaseResponse {
    @Expose
    public UserObject Data;

    public UserObject getData() {
        return Data;
    }

    public void setData(UserObject data) {
        this.Data = data;
    }
}
