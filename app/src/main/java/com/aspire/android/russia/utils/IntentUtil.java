package com.aspire.android.russia.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by nga.nguyent on 9/26/2016.
 */
public class IntentUtil {

    private static IntentUtil INSTANCE;

    private Context context;

    private IntentUtil(Context context) {
        this.context = context;
    }

    public static IntentUtil getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new IntentUtil(context);
        }
        return INSTANCE;
    }

    public void openCall(String phone) {
        /*if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/
        Uri uri = Uri.parse("tel:" + phone);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData( uri );
        context.startActivity(intent);
    }

    public void openDial(String phone) {
        Uri uri = Uri.parse("tel:" + phone);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData( uri );
        context.startActivity(intent);
    }

    /**
     * Func call all app in device can send message: mail, sms, mms, face, twitter,...
     * */
    public void shareAll(String message) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        //intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "subject");
        intent.setType("plain/text");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, message);

        // if (URI != null)
        // emailIntent.putExtra(Intent.EXTRA_STREAM, URI);

        context.startActivity(intent);
    }
    public void sendMail(String mail) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("mailto:" + mail));
        context.startActivity(intent);
    }


}
