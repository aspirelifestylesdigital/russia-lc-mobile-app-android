package com.aspire.android.russia.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.dcr.widgets.ExpandableTextView;
import com.aspire.android.russia.helper.CustomTypefaceSpan;
import com.aspire.android.russia.helper.FontHelper;
import com.aspire.android.russia.helper.HtmlTagHandler;
import com.aspire.android.russia.helper.IClickableUrlSpanListener;
import com.aspire.android.russia.helper.NoUnderlineURLSpan;
import com.aspire.android.russia.model.CurrencyObject;
import com.google.gson.Gson;
import com.google.gson.JsonNull;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    public static String FORMAT_DATE = "yyyy-MM-dd";

    /*
     * Make signature for url and params Inputs: url, list of params and its
	 * value, secret Output: md5 signature
	 */
//    public static String getSignatureWithURLParams(String url,
//                                                   HashMap<String, String> params,
//                                                   String consumerKey) {
//        String signature = "";
//        String encodeedParams = "";
//        if (params != null) {
//            List<Map.Entry<String, String>> sortedParams = sortParams(params);
//            // Log.d("J Bz", sortedParams.toString());
//            encodeedParams = URLEncoder
//                                     .encode(convertListNameValuePairToStringForSign(sortedParams));
//        }
//        signature = makeMd5(consumerKey + url.replaceAll("/",
//                                                         "") + encodeedParams);
//        // Log.d("J Bz", secret + url.replaceAll("/", "") + encodeedParams);
//        return signature;
//    }

    /*
         * Make signature for url and params Inputs: url, list of params and its
         * value, secret Output: md5 signature
         */
//    public static String getSignatureWithEncodingURLParams(String url,
//                                                           HashMap<String, String> params,
//                                                           String consumerKey) {
//        String signature = "";
//        String encodeedParams = "";
//        if (params != null) {
//            List<Map.Entry<String, String>> sortedParams = sortParams(params);
//            // Log.d("J Bz", sortedParams.toString());
//            encodeedParams = URLEncoder
//                                     .encode(convertListNameValuePairToStringForSign(sortedParams));
//
//        }
//
////        String encodeUrl = URLEncoder.encode(url.replaceAll("/", ""));
//        String encodeUrl = url.replaceAll("/",
//                                          "");
//        signature = makeMd5(consumerKey + encodeUrl.replaceAll("\\+",
//                                                               "%20") + encodeedParams);
//
//        return signature;
//    }

    /*
     * Make signature for url and json Inputs: url, json , secret Output: md5
     * signature
     */
//    public static String getSignatureWithJSONParams(String url,
//                                                    String jsonString,
//                                                    String consumerKey) {
//        return getSignatureWithJSONParams(url,
//                                          jsonString,
//                                          null,
//                                          consumerKey);
//    }

    /*
     * Make signature for url and json Inputs: url, json , secret Output: md5
     * signature
     */
//    public static String getSignatureWithJSONParams(String url,
//                                                    String jsonString,
//                                                    String sessionToken,
//                                                    String consumerKey) {
//        String signature = "";
//        // String jsonValue = json.toString();
//        jsonString = jsonString.replace("\n",
//                                        "")
//                               .replace("\r",
//                                        "");
//        //Log.d("Before", jsonString);
//        HashMap<String, Object> retMap = new Gson().fromJson(jsonString,
//                                                             new TypeToken<HashMap<String, Object>>() {
//                                                             }.getType());
//        if (sessionToken != null) {
//            retMap.put("session_token",
//                       sessionToken);
//        }
//
//        String encodeedParams = "";
//        if (retMap != null) {
//            List<Map.Entry<String, Object>> sortedParams = sortParamsObject(retMap);
//            // Log.d("J Bz", sortedParams.toString());
//            encodeedParams = URLEncoder
//                                     .encode(convertListNameObjectPairToStringForSign("",
//                                                                                      sortedParams));
//
//        }
//
//        String encodeUrl = URLEncoder.encode(url.replaceAll("/",
//                                                            ""));
//        //Log.d("After", consumerKey + encodeUrl.replaceAll("\\+", "%20") + encodeedParams);
//        signature = makeMd5(consumerKey + encodeUrl.replaceAll("\\+",
//                                                               "%20") + encodeedParams);


//        signature = makeMd5(consumerKey + url.replaceAll("/", "") + jsonString);
//        signature = getSignatureWithEncodingURLParams(url, retMap, consumerKey);
        // Log.d("J Bz", secret + url.replaceAll("/", "") + encodeedParams);
        // jsonValue = null;
//        System.gc();
//        return signature;
//    }

    /*
     * Sort params key before sign Input: list of params Output: sorted params
	 */
    public static List<Map.Entry<String, String>> sortParams(HashMap<String, String> params) {
        List<Map.Entry<String, String>> results = new ArrayList<>(params.entrySet());
        Collections.sort(results,
                         new Comparator<Map.Entry<String, String>>() {
                             public int compare(Map.Entry<String, String> o1,
                                                Map.Entry<String, String> o2) {
                                 return o1.getKey()
                                          .compareToIgnoreCase(o2.getKey());
                             }
                         });
        return results;
    }

    public static List<Map.Entry<String, Object>> sortParamsObject(HashMap<String, Object> params) {
        List<Map.Entry<String, Object>> results = new ArrayList<>(params.entrySet());
        Collections.sort(results,
                         new Comparator<Map.Entry<String, Object>>() {
                             public int compare(Map.Entry<String, Object> o1,
                                                Map.Entry<String, Object> o2) {
                                 return o1.getKey()
                                          .compareToIgnoreCase(o2.getKey());
                             }
                         });
        return results;
    }

    public static List<Map.Entry<String, Object>> sortParamsObject(LinkedTreeMap<String, Object> params) {
        List<Map.Entry<String, Object>> results = new ArrayList<>(params.entrySet());
        Collections.sort(results,
                         new Comparator<Map.Entry<String, Object>>() {
                             public int compare(Map.Entry<String, Object> o1,
                                                Map.Entry<String, Object> o2) {
                                 return o1.getKey()
                                          .compareToIgnoreCase(o2.getKey());
                             }
                         });
        return results;
    }

    public static String convertListNameValuePairToStringForSign(
                                                                        List<Map.Entry<String, String>> list) {
        String result = "";
        Map.Entry<String, String> item;
        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            result += item.getKey() + item.getValue();
        }
        return result;
    }

    public static String convertListNameObjectPairToStringForSign(String key,
                                                                  List<Map.Entry<String, Object>> list) {
        String result = "";
        Map.Entry<String, Object> item;
        List<Map.Entry<String, Object>> sortedParams;
        ArrayList listItems;
        String value = "";
        String actualKey;
        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            actualKey = (key != null && key.length() > 0) ?
                        String.format("%s[%s]",
                                      key,
                                      item.getKey()) :
                        item.getKey();
            if (item.getValue() instanceof HashMap) {
                sortedParams = sortParamsObject((HashMap<String, Object>) item.getValue());
                value = convertListNameObjectPairToStringForSign(actualKey,
                                                                 sortedParams);
            } else if (item.getValue() instanceof LinkedTreeMap) {
                sortedParams = sortParamsObject((LinkedTreeMap<String, Object>) item.getValue());
                value = convertListNameObjectPairToStringForSign(actualKey,
                                                                 sortedParams);
            } else if (item.getValue() instanceof ArrayList) {
                value = convertArrayListNameObjectToString(actualKey,
                                                           (ArrayList) item.getValue());
            } else if (item.getValue() instanceof String || item.getValue() instanceof Number) {
                value = actualKey + String.valueOf(item.getValue());
            }
            result += value;
        }
        return result;
    }

    public static String convertArrayListNameObjectToString(String key,
                                                            ArrayList<Object> list) {
        List<Map.Entry<String, Object>> sortedParams;
        String actualKey = "";
        String value;
        Object subItem;
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            subItem = list.get(i);
            actualKey = String.format("%s[%d]",
                                      key,
                                      i);
            value = "";
            if (subItem instanceof String) {
                value = actualKey + (String) subItem;
            } else if (subItem instanceof Number) {
                value = actualKey + String.valueOf(subItem);
            } else if (subItem instanceof HashMap) {
                sortedParams = sortParamsObject((HashMap<String, Object>) subItem);
                value = convertListNameObjectPairToStringForSign(actualKey,
                                                                 sortedParams);
            } else if (subItem instanceof LinkedTreeMap) {
                sortedParams = sortParamsObject((LinkedTreeMap<String, Object>) subItem);
                value = convertListNameObjectPairToStringForSign(actualKey,
                                                                 sortedParams);
            }
            result += value;
        }

        return result;
    }

//    public static String makeMd5(URL url) {
//        String secretString;
//        try {
//            MessageDigest digest = MessageDigest.getInstance("MD5");
//            byte[] md5hash = digest.digest(url.toString()
//                                              .getBytes());
//            BigInteger md5hex = new BigInteger(1,
//                                               md5hash);
//            secretString = md5hex.toString(16);
//        } catch (NoSuchAlgorithmException e) {
//            secretString = Long.toHexString(url.hashCode());
//            // Log.e("CommonUtil.urlToMd5String", e.getMessage());
//        }
//        return secretString;
//    }

//    public static String makeMd5(String stringBeforeSign) {
//        String secretSring;
//        try {
//            MessageDigest digest = MessageDigest.getInstance("MD5");
//            byte[] md5hash = digest.digest(stringBeforeSign.getBytes());
//            BigInteger md5hex = new BigInteger(1,
//                                               md5hash);
//            secretSring = md5hex.toString(16);
//            // Now we need to zero pad it if you actually want the full 32
//            // chars.
//            while (secretSring.length() < 32) {
//                secretSring = "0" + secretSring;
//            }
//        } catch (NoSuchAlgorithmException e) {
//            secretSring = Long.toHexString(stringBeforeSign.hashCode());
//            // Log.e("CommonUtil.urlToMd5String", e.getMessage());
//        }
//        return secretSring;
//    }

    public static String convertItemIdToURL(int itemId,
                                            String url) {
        String hex = Integer.toHexString(itemId);
        for (int i = hex.length(); i < 8; i++) {
            hex = "0" + hex;
        }
        String[] hexSplitted = new String[4];
        for (int i = 0; i < 4; i++) {
            hexSplitted[i] = hex.substring(2 * i,
                                           2 * i + 2);
        }

        for (int i = hexSplitted.length - 1; i >= 0; i--) {
            url += hexSplitted[i];
        }
        return url;
    }

    public static String convertReviewIdToHex(int reviewId) {
        String sUrl = "";

        String hex = Integer.toHexString(reviewId);
        for (int i = hex.length(); i < 8; i++) {
            hex = "0" + hex;
        }
        String[] hexSplitted = new String[4];
        for (int i = 0; i < 4; i++) {
            hexSplitted[i] = hex.substring(2 * i,
                                           2 * i + 2);
        }

        for (int i = hexSplitted.length - 1; i >= 0; i--) {
            sUrl += hexSplitted[i];
        }
        //sUrl += "/";
        return sUrl;
    }

    /*
	 * Get UTC by GMT
	 */
    public static long getGMTTime() {
        return (long) Math.floor((double) (System.currentTimeMillis() / 1000));
    }

    public static String removeNullValue(Object value) {
        if ((value == null)
                    || (value instanceof JsonNull)
                    || ((value instanceof String) && (((String) value)
                                                              .equalsIgnoreCase("null") || (((String) value)
                                                                                                    .equalsIgnoreCase("(null)"))))) {
            return "";
        }
        return (String) value;
    }

    public static boolean stringIsEmpty(String val) {
        if (val == null) {
            return true;
        }

        return val.isEmpty();
    }

    public static String getStringArrWithSeparator(List list,
                                                   String separator) {
        String result = "";
        if (list != null && list.size() > 0) {
            for (int index = 0; index < list.size(); index++) {
                if (index == list.size() - 1) {
                    result += list.get(index)
                                  .toString();
                } else {
                    result += list.get(index)
                                  .toString() + separator;
                }
            }
        }
        return result;
    }

    /**
     * Find all the items that contain that text
     */
    public static List<String> findAll(List<String> list,
                                       String text,
                                       int limit) {
        List<String> result = new ArrayList<>();
        for (String item : list) {
            if (item.toLowerCase()
                    .contains(text.toLowerCase()) &&
                        !result.contains(item)) {
                result.add(item);
                if (result.size() == limit) {
                    break;
                }
            }
        }
        return result;
    }

    /*
	 * Read an string from the assets directory
	 */
    public static String getStringFromAssets(Context context,
                                             String fileName) {
        String token = null;
        byte[] readBuffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        int bytesRead = 0;

        try {
            is = context.getAssets()
                        .open(fileName,
                              Context.MODE_PRIVATE);
            while ((bytesRead = is.read(readBuffer)) != -1) {
                baos.write(readBuffer,
                           0,
                           bytesRead);
            }
            baos.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        token = new String(baos.toByteArray());
        return token;
    }

    /**
     * Hide the keyboard for the attached view
     */
    public static void hideSoftKeyboard(Context context,
                                        View view) {
        if (view != null) {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboard(Context context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService (Context.INPUT_METHOD_SERVICE);

            View view = ((Activity) context).getCurrentFocus ();
            if (view != null) {
                inputManager.hideSoftInputFromWindow (view.getWindowToken (), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
        catch (Exception e) {
            e.printStackTrace ();
        }
        /*((Activity) context).getWindow()
                            .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/
    }

    public static void showSoftKeyboard(Context context,
                                        View view) {
        InputMethodManager imm =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view,
                          0);
    }

    public static String errorMessageAPI() {
        String errorMessage =
                "Sorry, an error was encountered while processing your request. Please try again later.";
		/*
		 * switch(resultCode) { case 400: errorMessage =
		 * "Failure. See \"error\" in response for the reason."; break; case
		 * 401: errorMessage =
		 * "Unauthorized, i.e. authentication is unsuccessful or session token is invalid."
		 * ; break; case 403: errorMessage =
		 * "Forbidden, i.e. you do not have access to this API."; break; case
		 * 404: errorMessage = "Not Found."; break; case 419: errorMessage =
		 * "Session token timeout. Please re-runSignAuthenticate with Authenticate API."
		 * ; break; case 500: errorMessage = "Server Error."; break; case 503:
		 * errorMessage = "Service unavailable."; break; default: break; }
		 */
        return errorMessage;

    }

    /**
     * Checking for all possible internet providers
     **/
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                                                                         .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                       activeNetwork.isConnected();
    }

    public static int getScreenHeight(Activity activity) {
        if(SCREEN_HEIGHT == 0) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(metrics);

            SCREEN_HEIGHT = metrics.heightPixels;
        } return SCREEN_HEIGHT;
    }

    public static int getScreenWidth(Activity activity) {
        if(SCREEN_WIDTH == 0) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(metrics);
            SCREEN_WIDTH = metrics.widthPixels;
        }
        return SCREEN_WIDTH;
    }

    public static void showActionAlertOnlyYESDontCancel(Context context,
                                                        String header,
                                                        String message,
                                                        String positiveLabel,
                                                        DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (header != null && header.length() != 0) {
            builder.setTitle(header);
        }
        builder.setMessage(message)
               .setCancelable(false)
               .setPositiveButton(positiveLabel,
                                  positiveAction);
        if (positiveAction != null) {
            builder.setPositiveButton(positiveLabel,
                                      positiveAction);
        } else {
            builder.setPositiveButton(positiveLabel,
                                      new DialogInterface.OnClickListener() {

                                          @Override
                                          public void onClick(DialogInterface dialog,
                                                              int which) {
                                              // TODO Auto-generated method stub
                                              dialog.dismiss();
                                          }
                                      });
        }
        Dialog dialog = builder.show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    public static void showActionAlertOnlyYES(Context context,
                                              String header,
                                              String message,
                                              String positiveLabel,
                                              DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (header != null && header.length() != 0) {
            builder.setTitle(header);
        }
        builder.setMessage(message)
               .setCancelable(true)
               .setPositiveButton(positiveLabel,
                                  positiveAction);
        if (positiveAction != null) {
            builder.setPositiveButton(positiveLabel,
                                      positiveAction);
        } else {
            builder.setPositiveButton(positiveLabel,
                                      new DialogInterface.OnClickListener() {

                                          @Override
                                          public void onClick(DialogInterface dialog,
                                                              int which) {
                                              // TODO Auto-generated method stub
                                              dialog.dismiss();
                                          }
                                      });
        }
        Dialog dialog = builder.show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    public static void showActionAlertYESORNO(Context context,
                                              String header,
                                              String message,
                                              String positiveLabel,
                                              DialogInterface.OnClickListener positiveAction,
                                              String negativeLabel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (header != null && header.length() != 0) {
            builder.setTitle(header);
        }


        Dialog dialog = builder.setMessage(message)
                               .setCancelable(true)
                               .setPositiveButton(positiveLabel,
                                                  positiveAction)
                               .setNegativeButton(negativeLabel,
                                                  new DialogInterface.OnClickListener() {
                                                      public void onClick(DialogInterface dialog,
                                                                          int id) {
                                                          dialog.cancel();
                                                      }
                                                  })
                               .show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    public static AlertDialog showActionAlertYESORNO(Context context,
                                                     String header,
                                                     String message,
                                                     String positiveLabel,
                                                     DialogInterface.OnClickListener positiveAction,
                                                     String negativeLabel,
                                                     DialogInterface.OnClickListener negativeAction) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (header != null && header.length() != 0) {
            builder.setTitle(header);
        }
        AlertDialog dialog = builder.setMessage(message)
                                    .setCancelable(true)
                                    .setPositiveButton(positiveLabel,
                                                       positiveAction)
                                    .setNegativeButton(negativeLabel,
                                                       negativeAction)
                                    .show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);

        return dialog;
    }

    public static AlertDialog showActionAlertNotification(Context context,
                                                          String titile,
                                                          String message,
                                                          final View.OnClickListener onOkClick,
                                                          final View.OnClickListener onCancelClick) {

        //Theme_DeviceDefault_Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(context,
                                                              android.R.style.Theme_Material_Light_Dialog);

        if (titile != null && titile.length() != 0) {
            builder.setTitle(titile);
        }

        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                                  null);
        builder.setNegativeButton(android.R.string.cancel,
                                  null);

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                /*ColorDrawable drawable = new ColorDrawable();
                drawable.setColor(Color.WHITE);

                Window view = dialog.getWindow();
                view.setBackgroundDrawable(drawable);*/

                Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(onOkClick);

                Button btnCancel = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnCancel.setOnClickListener(onCancelClick);
            }
        });

        dialog.show();

        /*TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);*/

        return dialog;
    }

    public static void showActionAlertYESORNODontCancel(Context context,
                                                        String header,
                                                        String message,
                                                        String positiveLabel,
                                                        DialogInterface.OnClickListener positiveAction,
                                                        String negativeLabel,
                                                        DialogInterface.OnClickListener negativeAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (header != null && header.length() != 0) {
            builder.setTitle(header);
        }
        Dialog dialog = builder.setMessage(message)
                               .setCancelable(false)
                               .setPositiveButton(positiveLabel,
                                                  positiveAction)
                               .setNegativeButton(negativeLabel,
                                                  negativeAction)
                               .show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    public static void showNetworkConnectionError(Context context,
                                                  DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You are offline please check your internet connection")
               .setCancelable(false)
               .setPositiveButton("OK",
                                  positiveAction);
        builder.show();

    }

    public static AlertDialog showNetworkConnection(Context context,
                                                    DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You are offline please check your internet connection")
               .setCancelable(false)
               .setPositiveButton("OK",
                                  positiveAction);

        return builder.create();

    }
    public static void showDialogRequiredSignIn(Context context,
                                         DialogInterface.OnClickListener positiveAction,
                                         DialogInterface.OnClickListener negativeAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(context.getString(R.string.text_title_dialog_sign_in_required));

        AlertDialog dialog =
                builder.setMessage(context.getString(R.string.text_message_dialog_sign_in_required))
                       .setCancelable(true)
                       .setPositiveButton(context.getString(R.string.text_dialog_sign_in),positiveAction)
                       .setNegativeButton(
                        context.getString(R.string.text_dialog_cancel),negativeAction
                               )
                       .show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
        Button btnSignIn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        btnSignIn.setAllCaps(false);

        Button btnCancel = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        btnCancel.setAllCaps(false);
        TextView dialogTitle = (TextView)dialog.findViewById(android.R.id.title);
        CommonUtils.setFontForViewRecursive(dialogTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(messageText,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(btnSignIn,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD);
        CommonUtils.setFontForViewRecursive(btnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD);
        dialog.show();


    }

    /**
     * 123456 --> 123,456
     * 1000000 --> 1,000,000
     */
    public static String getDisplayCurrencyFormat(int currency) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        return numberFormat.format(currency);
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker,
                                                   int color) {
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            child.setLongClickable(false);
            child.setOnLongClickListener(null);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                                                                .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                } catch (IllegalAccessException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        return false;
    }

    public static String getSpacingPhoneNumberWithCountryCode(String phone) {

        if (phone != null && phone.length() > 0) {
            String originalPhone = phone.replace("+65",
                                                 "")
                                        .
                                                replace("+60",
                                                        "")
                                        .replace("+",
                                                 "")
                                        .replace(" ",
                                                 "");
            String res = "";
            while (originalPhone.length() > 4) {
                res += originalPhone.substring(0,
                                               4) + " ";
                originalPhone = originalPhone.substring(4,
                                                        originalPhone.length());
            }
            if (originalPhone.length() > 0) {
                res += originalPhone;
            }
            //TODO: add channel
//            if(RestClient.getCHANNEL().equals("sg")){
//                res = "+65 " + res;
//            }else{
//                res = "+60 " + res;
//            }
            return res;
        }
        return phone;
    }

    public static String formatDate(Date date,
                                    String format) {
        SimpleDateFormat df = new SimpleDateFormat(format,
                                                   Locale.ENGLISH);
        String result = df.format(date);
        return result;
    }

    public static String formatTimeSlot(int timeslotId) {
        if (timeslotId >= 0) {
            String format = "";

            String timeslot = timeslotId + "";
            String hour = "";
            String minute = "";

            if (timeslot.length() > 3) {
                minute = timeslot.substring(2,
                                            4);
                hour = timeslot.substring(0,
                                          2);
            } else if (timeslot.length() > 2) {
                minute = timeslot.substring(1,
                                            3);
                hour = timeslot.substring(0,
                                          1);
            } else {
                // return format;
                hour = "00";
                minute = (timeslotId >= 10) ?
                         timeslot :
                         "0" + timeslot;
            }

            Calendar cal = Calendar.getInstance();

            cal.set(Calendar.HOUR_OF_DAY,
                    Integer.valueOf(hour));
            cal.set(Calendar.MINUTE,
                    Integer.valueOf(minute));
            long offset = cal.get(Calendar.ZONE_OFFSET)
                                  + cal.get(Calendar.DST_OFFSET);
            long realTime = cal.getTimeInMillis() + offset;
            Date date = new Date(cal.getTimeInMillis());

            SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
            format = df.format(date);

            return format;
        }
        return "";
    }

    public static Boolean isSameDay(Date date1,
                                    Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                       cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static int convertPxToDp(int px,
                                    Context context) {
        DisplayMetrics displayMetrics = context.getResources()
                                               .getDisplayMetrics();
        return (int) (px / displayMetrics.density);
    }

    public static int convertDpToPx(int dp,
                                    Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                               dp,
                                               context.getResources()
                                                      .getDisplayMetrics());
    }

    public static Map<String, String> parseUriQueryToMap(Uri uri) {
        Map<String, String> results = new HashMap<String, String>();
        try {
            String query = uri.getEncodedQuery();
            if (query != null) {
                String[] params = query.split("&");
                for (String value : params) {
                    int idx = value.indexOf("=");
                    results.put(URLDecoder.decode(value.substring(0,
                                                                  idx),
                                                  "UTF-8"),
                                URLDecoder.decode(value.substring(idx + 1),
                                                  "UTF-8"));
                }
            }
        } catch (Exception e) {
        }
        return results;
    }

    public static String pad3(int c) {
        if (c == 12) {
            return " PM";
        }
        if (c == 00) {
            return " AM";
        }
        if (c > 12) {
            return " PM";
        } else {
            return " AM";
        }
    }

    public static String getCurrencyString(boolean shortType) {
//        if(RestClient.getCHANNEL()!=null){
//            return RestClient.getCHANNEL().equals("sg") ? (shortType ? "$" : "SGD") : "RM";
//        }
        return "";
    }

    /**
     * Parse to string with html format
     */
    public static Spanned parseHtmlStringFormat(String text) {
        if (text != null) {
            text = text.replace("\r\n",
                                "<br/>")
                       .replace("\n\r",
                                "<br/>")
                       .replace("\n",
                                "<br/>")
                       .replace("\r",
                                "<br/>");
            return Html.fromHtml(text);
        }
        return null;
    }

    public static String removeAllHyperlink(String text) {
        if (text != null) {
            return text.replaceAll("</?a[^>]*>",
                                   "");
        }
        return null;
    }

    public static String escapeHtmlTag(String html) {
        if (html != null) {
            html = html.replaceAll("<(\"[^\"]*\"|'[^']*'|[^'\">])*>",
                                   "");//Removes all items in brackets
//            html = html.replaceAll("</(.*)>"," ");//Must be undeneath
//            html = html.replaceAll("&nbsp;"," ");
//            html = html.replaceAll("&amp;"," ");
            return html;
        }
        return null;
    }

    public static int getTheCenterIndex(int count) {
        int dividerBy = count / 2;
        int remainder = count % 2;
        return (dividerBy + (remainder - 1));
    }

    public static boolean isOpenedFromDifferenceChannel() {
//        if (PushNotificationUtils.CURRENT_CHANNEL != null && !PushNotificationUtils
//                .CURRENT_CHANNEL.equals(RestClient.getCHANNEL())) {
//            return true;
//        }
        return false;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Set view or sub views with custom font.
     *
     * @param rootView
     *         The root view.
     * @param fontFileName
     *         The full path of custom font in asset folder.
     * @since v0.1
     */
    public static void setFontForViewRecursive(final View rootView,
                                               final String fontFileName) {
        if (rootView instanceof ViewGroup) {
            // Let's set sub views fonts.
            final ViewGroup viewGroup = (ViewGroup) rootView;
            final int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                CommonUtils.setFontForViewRecursive(viewGroup.getChildAt(i),
                                                    fontFileName);
            }
        } else if (rootView instanceof TextView) {
            CommonUtils.setFontForTextView((TextView) rootView,
                                           fontFileName);
        }
    }
    public static void setFontForViewRecursive(final View rootView,
                                               final String fontFileName, int style) {
        if (rootView instanceof ViewGroup) {
            // Let's set sub views fonts.
            final ViewGroup viewGroup = (ViewGroup) rootView;
            final int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                CommonUtils.setFontForViewRecursive(viewGroup.getChildAt(i),
                        fontFileName, style);
            }
        } else if (rootView instanceof TextView) {
            CommonUtils.setFontForTextView((TextView) rootView,
                    fontFileName, style);
        }
    }
    public static void setFontForViewList(String fontFileName, View... views){
        if(views != null && views.length > 0){
            for(View view : views){
                if(view instanceof TextView){
                    setFontForTextView((TextView)view, fontFileName);
                }else if(view instanceof Button){
                    setFontForTextView((Button)view, fontFileName);
                }else if(view instanceof CheckBox){
                    setFontForCheckbox((CheckBox)view, fontFileName);
                }
            }
        }
    }
    public static void setFontForViewList(String fontFileName, int type, View... views){
        if(views != null && views.length > 0){
            for(View view : views){
                if(view instanceof TextView){
                    setFontForTextView((TextView)view, fontFileName, type);
                }else if(view instanceof Button){
                    setFontForTextView((Button)view, fontFileName, type);
                }else if(view instanceof CheckBox){
                    setFontForCheckbox((CheckBox)view, fontFileName, type);
                }
            }
        }
    }
    public static void applyFontToMenuItem(Context context,MenuItem mi,final String fontFileName) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), fontFileName);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
    public static void setFontForButton(final Button button,
                                          final String fontFileName) {
        // Check input data.
        if ((button == null) || CommonUtils.stringIsEmpty(fontFileName)) {
            return;
        }

        // Check working context.
        if (button.getContext() == null) {
            return;
        }

        // Set custom font.
        button.setTypeface(FontHelper.get(button.getContext(),
                fontFileName),
                Typeface.NORMAL);
    }
    /**
     * Set custom font for text view.
     *
     * @param textView
     *         The text view which uses custom fonts.
     * @param fontFileName
     *         The full path of custom font in asset folder.
     * @since v0.1
     */
    public static void setFontForTextView(final TextView textView,
                                          final String fontFileName) {
        setFontForTextView(textView, fontFileName, Typeface.NORMAL);

    }
    public static void setFontForTextView(final TextView textView,
                                          final String fontFileName, int style) {
        // Check input data.
        if ((textView == null) || CommonUtils.stringIsEmpty(fontFileName)) {
            return;
        }

        // Check working context.
        if (textView.getContext() == null) {
            return;
        }

        // Set custom font.
        textView.setTypeface(FontHelper.get(textView.getContext(),
                fontFileName),
                style);

    }
    public static void setFontForCheckbox(final CheckBox textView,
                                          final String fontFileName, int style) {
        // Check input data.
        if ((textView == null) || CommonUtils.stringIsEmpty(fontFileName)) {
            return;
        }

        // Check working context.
        if (textView.getContext() == null) {
            return;
        }

        // Set custom font.
        textView.setTypeface(FontHelper.get(textView.getContext(),
                fontFileName),
                style);
    }
    public static void setFontForCheckbox(final CheckBox textView,
                                          final String fontFileName) {
        // Check input data.
        if ((textView == null) || CommonUtils.stringIsEmpty(fontFileName)) {
            return;
        }

        // Check working context.
        if (textView.getContext() == null) {
            return;
        }

        // Set custom font.
        textView.setTypeface(FontHelper.get(textView.getContext(),
                fontFileName),
                Typeface.NORMAL);
    }
    public static void setLineSpacingExtraForTextViewRecursive(final View rootView) {
        if (rootView instanceof ViewGroup) {
            // Let's set sub views fonts.
            final ViewGroup viewGroup = (ViewGroup) rootView;
            final int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                CommonUtils.setLineSpacingExtraForTextViewRecursive(viewGroup.getChildAt(i));
            }
        } else if (rootView instanceof TextView) {
            String text = ((TextView)rootView).getText().toString();
            ((TextView)rootView).setLineSpacing(rootView.getContext().getResources().getDimension(R.dimen.line_spacing_extra), 1);
            if(!TextUtils.isEmpty(text) && text.contains("*")){
                ((TextView)rootView).setText(TextUtil.makeBoldStarOnText(text));
            }
        }
    }
    public static void makeFirstCharacterUpperCase(EditText... editTextList){
        for(EditText editText : editTextList) {
            /*editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    Log.d("ThuNguyen", "afterTextChanged");
                    StringUtil.makeFirstLetterUpperCase(editText);
                    //editText.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
                }
            });*/
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }
    }
    public static boolean secretValidator(String secretWord) {
        /*Pattern pattern;
        Matcher matcher;
        if(secretWord!=null && secretWord.trim().length()>0) {
            final String SECRET_PATTERN =
                    "^[0-9a-zA-Z]{1,5}$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(secretWord);
            return matcher.matches();
        } else {
          return false;
        }*/
        return CommonUtils.isStringValid(secretWord);
    }
    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }
    /**
     * Setup spandable string for TextView.
     *
     * @param view
     *         The original TextView.
     * @param text
     *         The text will be displayed.
     * @since v0.3
     */
    public static void setupTextViewSpannableString(final TextView view,
                                                    final String text,
                                                    final IClickableUrlSpanListener urlListener) {
        if (view == null) {
            return;
        }

        view.setText(CommonUtils.getStringData(text));

        if (view.getText()
                .length() == 0) {
            return;
        }

        Linkify.addLinks(view,
                         Linkify.ALL);

        final Spannable spannable = new SpannableString(view.getText());

        final URLSpan[] urls = spannable.getSpans(0,
                                                  spannable.length(),
                                                  URLSpan.class);

        for (final URLSpan span : urls) {
            final int start = spannable.getSpanStart(span);
            final int end = spannable.getSpanEnd(span);
            final int flag = spannable.getSpanFlags(span);

            spannable.removeSpan(span);

            spannable.setSpan(new NoUnderlineURLSpan(span.getURL(),
                                                     urlListener),
                              start,
                              end,
                              flag);
        }

        view.setText(spannable);
    }

    public static String getStringData(final String data) {
        return (data != null) ?
               data :
               "";
    }

    public static String getCountryCode(String countryCode) {
        if (countryCode == null) {
            return "";
        } else {
            if(countryCode.contains("(")&& countryCode.contains(")")) {
                return "+" + countryCode.substring(countryCode.lastIndexOf("(") + 1,
                                                   countryCode.lastIndexOf(")"));
            }else {
                return "";
            }
        }
    }

    public static String[] splitCountryCodeFromMobileNumber(Context context, String phone) {
        String[] resource = context.getResources().getStringArray(R.array.country_arrays);
        String countryCode = "";
        String mobileNumber = "";
        String resData = "";

        if(!phone.contains("+")){
            phone = "+" + phone;
        }

        for (String s : resource) {
            String cc = CommonUtils.getCountryCode(s);
            if (phone.startsWith(cc)) {
                if(cc.length()>countryCode.length()){
                    countryCode = cc;
                    mobileNumber = phone.substring(countryCode.length());
                    resData = s;
                }
            }
        }

        String[]phones = new String[]{countryCode, mobileNumber, resData};
        return phones;
    }

    public static boolean isStringValid(String val) {
        if (val == null || val.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public static String epoch2DateString(long epochSeconds,
                                          String formatString) {
        if(formatString==null){
            formatString ="dd-MMM-yyyy";
        }
        Date updatedate = new Date(epochSeconds * 1000);
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        return format.format(updatedate);
    }
    public static Date epoch2DateString(long epochSeconds) {
        return new Date(epochSeconds * 1000);
    }

    /**
     * get time utc with date string.
     *
     * @param dateString
     *         10-Otc-2016.
     * @return time
     * @since v0.1
     */
    public static Long getUTCTime(String dateString) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = format.parse(dateString);
            String dateAsString = format.format(date);
            System.out.println(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime() / 1000;
    }

    public static Long getUTCTime(int year, int month, int day){
        Date myDate = new Date(year, month, day);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(year, month, day);

        Date time = calendar.getTime();

        SimpleDateFormat outputFmt = new SimpleDateFormat("dd-MMM-yyyy h:mm a zz"/*"MMM dd, yyy h:mm a zz"*/);
        String dateAsString = outputFmt.format(time);
        System.out.println(dateAsString);
        System.out.println(outputFmt.format(myDate));

        return time.getTime();
    }
    public static String format2Character(int number){
        String result = "";
        if(number>9){
            result+=number;
        }else {
            result = "0"+number;
        }
        return result;
    }


    public static String getMonthName(int month) {
        switch (month + 1) {
            case 1:
                return "Jan";

            case 2:
                return "Feb";

            case 3:
                return "Mar";

            case 4:
                return "Apr";

            case 5:
                return "May";

            case 6:
                return "Jun";

            case 7:
                return "Jul";

            case 8:
                return "Aug";

            case 9:
                return "Sep";

            case 10:
                return "Oct";

            case 11:
                return "Nov";

            case 12:
                return "Dec";
        }

        return "";
    }

    public static String getBase64(Bitmap bitmap){
        if(bitmap==null)
            return "";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        //to encode base64 from byte array use following method

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }
    public static String getTimeFormat(int hour,int minute){
        String am = CommonUtils.pad3(hour);
        String text = format2Character( hour % 12) + ":" + format2Character(minute) + am;
        return text;
    }
    public static String getCountryCodeFromPhone(Context context,String phone) {
        String[] resource = context.getResources().getStringArray(R.array.country_arrays);
        String countrCode = "";
        String countryCodeSelected="";
        if(!phone.contains("+")){
            phone="+"+phone;
        }
        for (String s : resource) {
            if (phone.startsWith(CommonUtils.getCountryCode(s))) {

                String countryNumber = CommonUtils.getCountryCode(s);
                if(countryNumber.length()>countrCode.length()){
                    countrCode = CommonUtils.getCountryCode(s);
                    countryCodeSelected = s;
                }

            }
        }
        return countryCodeSelected;
    }

    public static boolean validateFutureDate(Context mContext,int day,int month,int year){
        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);
        int currentDay = c.get(Calendar.DAY_OF_MONTH);
        if (day < currentDay && year == currentYear && month == currentMonth) {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.text_concierge_booking_message_invalid_date), Toast.LENGTH_LONG).show();
            return false;
        } else if (month < currentMonth && year == currentYear) {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.text_concierge_booking_message_invalid_month), Toast.LENGTH_LONG).show();
            return false;
        } else if (year < currentYear) {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.text_concierge_booking_message_invalid_year), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
    public static boolean validateFutureDate(int day,int month,int year,int currentDay,int currentMonth,int currentYear){
        if (day < currentDay && year == currentYear && month == currentMonth) {
            return false;
        } else if (month < currentMonth && year == currentYear) {
            return false;
        } else if (year < currentYear) {
            return false;
        }

        return true;
    }
    public static boolean validateFutureDate(int day,int month,int year,Calendar minDate){
        int currentYear = minDate.get(Calendar.YEAR);
        int currentMonth = minDate.get(Calendar.MONTH);
        int currentDay = minDate.get(Calendar.DAY_OF_MONTH);
        return validateFutureDate(day, month, year, currentDay, currentMonth, currentYear);
    }
    public static boolean validateFutureDate(int day,int month,int year,DatePicker datePicker){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(datePicker.getMinDate());
        return validateFutureDate(day, month, year, calendar);
    }
    public static void convertHtmlToStr(String valHtml, TextView tvShow) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvShow.setText(Html.fromHtml(valHtml, Html.FROM_HTML_MODE_LEGACY));
        } else {
            tvShow.setText(Html.fromHtml(valHtml));
        }
    }
    public static void convertHtmlToStr(String valHtml, ExpandableTextView expandableTextView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            expandableTextView.setText(Html.fromHtml(valHtml, Html.FROM_HTML_MODE_LEGACY, null, new HtmlTagHandler()));
        } else {
            expandableTextView.setText(Html.fromHtml(valHtml, null, new HtmlTagHandler()));
        }
    }
    public static void convertHtmlShowHtml(String valHtml, TextView tvShow) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvShow.setText(Html.fromHtml(valHtml, Html.FROM_HTML_MODE_LEGACY));
        } else {
            tvShow.setText(Html.fromHtml(valHtml));
        }
    }

//    public static String getDeviceID(Context context) {
//
//        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        WifiInfo info = manager.getConnectionInfo();
//        String address = info.getMacAddress() + getDeviceName() + Build.SERIAL;
//        String deviceId = convertMd5(address);
//        return deviceId;
//    }

    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }
    public static void clearFocusLineSearchView(SearchView searchView) {
        try {
            if (null == searchView) {
                return;
            }
            /*int searchPlateId = searchView.getContext()
                    .getResources()
                    .getIdentifier("android:id/search_plate",
                            null,
                            null);*/
            View searchPlateView = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            if (searchPlateView != null) {
                searchPlateView.setBackgroundColor(Color.TRANSPARENT);
            }
            SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchAutoComplete.setTextSize(TypedValue.COMPLEX_UNIT_PX, searchView.getResources().getDimension(R.dimen.text_size_14sp));
            CommonUtils.setFontForViewRecursive(searchAutoComplete, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
//    private static String convertMd5(String res) {
//        String secretWord = null;
//        if (res != null) {
//            MessageDigest mdEnc;
//            try {
//                mdEnc = MessageDigest.getInstance("MD5");
//                mdEnc.update(res.getBytes(), 0, res.length());
//                res = new BigInteger(1, mdEnc.digest()).toString(16);
//                while (res.length() < 32) {
//                    res = "0" + res;
//                }
//                secretWord = res;
//            } catch (NoSuchAlgorithmException e1) {
//                e1.printStackTrace();
//            }
//        }
//
//        return secretWord;
//    }

    public static void resetManageUserPreference(){
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REQUEST_TOKEN, "");
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, "");
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_EXPIRED_AT, 0L);
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_EXPIRATION_TIME_DURATION, 0L);
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");

    }
    public static List<CurrencyObject> getCurrencyEn(){
            List<CurrencyObject> data = new ArrayList<>();
            CurrencyObject currencyObjectPopular = new CurrencyObject();
            currencyObjectPopular.setCurrencyKey("");
            currencyObjectPopular.setCurrencyText("Popular");
            data.add(currencyObjectPopular);
            CurrencyObject currencyObject = new CurrencyObject();
            currencyObject.setCurrencyKey("USD");
            currencyObject.setCurrencyText("US dollar");
            data.add(currencyObject);
            CurrencyObject currencyObject1 = new CurrencyObject();
            currencyObject1.setCurrencyKey("EUR");
            currencyObject1.setCurrencyText("Euro");
            data.add(currencyObject1);
            CurrencyObject currencyObject2 = new CurrencyObject();
            currencyObject2.setCurrencyKey("SGD");
            currencyObject2.setCurrencyText("Singapore");
            data.add(currencyObject2);
            CurrencyObject currencyObjectAllCurrency = new CurrencyObject();
            currencyObjectAllCurrency.setCurrencyKey("");
            currencyObjectAllCurrency.setCurrencyText("All currencies");
            data.add(currencyObjectAllCurrency);
            CurrencyObject currencyObject3 = new CurrencyObject();
            currencyObject3.setCurrencyKey("AUD");
            currencyObject3.setCurrencyText("Australian dollar");
            data.add(currencyObject3);
            CurrencyObject currencyObject5 = new CurrencyObject();
            currencyObject5.setCurrencyKey("CAD");
            currencyObject5.setCurrencyText("Canadian dollar");
            data.add(currencyObject5);
            CurrencyObject currencyObject6 = new CurrencyObject();
            currencyObject6.setCurrencyKey("CNY");
            currencyObject6.setCurrencyText("Chinese yuan");
            data.add(currencyObject6);
            CurrencyObject currencyObject7 = new CurrencyObject();
            currencyObject7.setCurrencyKey("EGP");
            currencyObject7.setCurrencyText("Egyptian pound");
            data.add(currencyObject7);
            CurrencyObject currencyObject8 = new CurrencyObject();
            currencyObject8.setCurrencyKey("HKD");
            currencyObject8.setCurrencyText("Hong Kong dollar");
            data.add(currencyObject8);
            CurrencyObject currencyObject9 = new CurrencyObject();
            currencyObject9.setCurrencyKey("Rs.");
            currencyObject9.setCurrencyText("Indian rupee");
            data.add(currencyObject9);
            CurrencyObject currencyObject10 = new CurrencyObject();
            currencyObject10.setCurrencyKey("Rp");
            currencyObject10.setCurrencyText("Indonesia rupiah");
            data.add(currencyObject10);
            CurrencyObject currencyObject11 = new CurrencyObject();
            currencyObject11.setCurrencyKey("¥");
            currencyObject11.setCurrencyText("Japanese yen");
            data.add(currencyObject11);
            CurrencyObject currencyObject12 = new CurrencyObject();
            currencyObject12.setCurrencyKey("KRW");
            currencyObject12.setCurrencyText("Korean won");
            data.add(currencyObject12);
            CurrencyObject currencyObject23 = new CurrencyObject();
            currencyObject23.setCurrencyKey("KWD");
            currencyObject23.setCurrencyText("Kuwaiti diar");
            data.add(currencyObject23);
            CurrencyObject currencyObject13 = new CurrencyObject();
            currencyObject13.setCurrencyKey("MYR");
            currencyObject13.setCurrencyText("Malaysia ringgit");
            data.add(currencyObject13);
            CurrencyObject currencyObject14 = new CurrencyObject();
            currencyObject14.setCurrencyKey("MXN");
            currencyObject14.setCurrencyText("Mexican peso");
            data.add(currencyObject14);
            CurrencyObject currencyObject15 = new CurrencyObject();
            currencyObject15.setCurrencyKey("TWD");
            currencyObject15.setCurrencyText("New Taiwan dollar");
            data.add(currencyObject15);
            CurrencyObject currencyObject16 = new CurrencyObject();
            currencyObject16.setCurrencyKey("£");
            currencyObject16.setCurrencyText("Pound sterling");
            data.add(currencyObject16);
            CurrencyObject currencyObject17 = new CurrencyObject();
            currencyObject17.setCurrencyKey("RUB");
            currencyObject17.setCurrencyText("Russian ruble");
            data.add(currencyObject17);
            CurrencyObject currencyObject18 = new CurrencyObject();
            currencyObject18.setCurrencyKey("SAR");
            currencyObject18.setCurrencyText("Saudi Arabian riyal");
            data.add(currencyObject18);
            CurrencyObject currencyObject19 = new CurrencyObject();
            currencyObject19.setCurrencyKey("ZAR");
            currencyObject19.setCurrencyText("South African rand");
            data.add(currencyObject19);
            CurrencyObject currencyObject20 = new CurrencyObject();
            currencyObject20.setCurrencyKey("SEK");
            currencyObject20.setCurrencyText("Swedish krona");
            data.add(currencyObject20);
            CurrencyObject currencyObject21 = new CurrencyObject();
            currencyObject21.setCurrencyKey("CHF");
            currencyObject21.setCurrencyText("Swiss franc");
            data.add(currencyObject21);
            CurrencyObject currencyObject22 = new CurrencyObject();
            currencyObject22.setCurrencyKey("THB");
            currencyObject22.setCurrencyText("Thai baht");
            data.add(currencyObject22);
            return data;
        }
        public static String getRusianString(String text, String[] listEn, String[] listRu){
            String result="";
            if(!isStringValid(text)||listEn==null || listRu== null || listEn.length!=listRu.length){
                return result;
            }
            int index = -1;
            for(int i= 0;i<listEn.length;i++){
                if(text.equals(listEn[i])){
                    index = i;
                    break;
                }
            }
            if(index>-1 && listRu.length>index){
                result = listRu[index];
            }

            return result;
        }
    public static String getRusianCussineString(String cussine){

        return getRusianString(cussine,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_cuisine_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.cuisine_array));
        //return cussine;
    }
    public static String getRusianOccasionString(String occasion){

        return getRusianString(occasion,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_occasion_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.occasion_array));
        //return occasion;
    }
    public static String getRusianEventCategoryString(String eventCategory){

        return getRusianString(eventCategory,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_event_category_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.event_category_array));
        //return eventCategory;
    }
    public static String getRusianBedPreferenceString(String bed_preference){

        return getRusianString(bed_preference,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_bed_preference_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.bed_preference_array));
        //return bed_preference;
    }
    public static String getRusiaRoomPreferenceString(String room_preference){

        return getRusianString(room_preference,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_room_preference_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.room_preference_array));
        //return room_preference;
    }
    public static String getRusiaTransportTypeString(String transport_type){

        return getRusianString(transport_type,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_transport_type_array),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.transport_type_array));
        //return transport_type;
    }
    public static String getRusiaPreferredRentalString(String preferred_rental){

        return getRusianString(preferred_rental,
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.en_preferred_rental_company),
                               AppContext.getSharedInstance().getResources().getStringArray(R.array.preferred_rental_company));
        //return preferred_rental;
    }
    public static String getRusiaPreferredVehicleString(String preferred_vehicle) {

        return getRusianString(preferred_vehicle,
                               AppContext.getSharedInstance()
                                         .getResources()
                                         .getStringArray(R.array.en_preferred_vehicle_array),
                               AppContext.getSharedInstance()
                                         .getResources()
                                         .getStringArray(R.array.preferred_vehicle_array));
        //return preferred_vehicle;
    }

    public static boolean isUrlLink(String url){
        return url.startsWith("http");
    }
    public static String replaceStringForHTMLDisplay(String source){

        return source.replace("\r\n", "<br/>").replace("\n", "<br/>");
    }
    public static String appendTwoLineForImageTag(String source){
        // Find the <img> tag content
        int imgTagInd = source.indexOf("<img");
        if(imgTagInd > -1){
            int imgTagEnd = source.indexOf("/>");
            String imgValue = source.substring(imgTagInd, imgTagEnd + 2);
            return source.replace(imgValue, (imgValue + "</p>\r\n\r\n")).replace("</p>\r\n\r\n</p>\r\n\r\n", "</p>\r\n\r\n");

        }
        return source;
    }

    public static String convertSalutationToRussian(String enValue){
        if(enValue!=null){
            if(enValue.equalsIgnoreCase("Mr.")){
                return "Г-н.";
            } else if (enValue.equalsIgnoreCase("Mrs.")){
                return "Г-жа.";
            }
        }
        return enValue;
    }

    public static String convertSalutationToEnglish(String ruValue){
        if(ruValue!=null){
            if(ruValue.equalsIgnoreCase("Г-н.")){
                return "Mr.";
            } else if (ruValue.equalsIgnoreCase("Г-жа.")){
                return "Mrs.";
            }
        }
        return ruValue;
    }
}
