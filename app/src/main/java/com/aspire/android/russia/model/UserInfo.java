package com.aspire.android.russia.model;


import com.google.gson.annotations.Expose;

public class UserInfo {
    @Expose
    private UserItem UserInfo;


    public UserItem getUserItem() {
        return UserInfo;
    }

    public void setUserItem(UserItem UserObject) {
        UserInfo = UserObject;
    }


}