package com.aspire.android.russia.apiservices;

import android.support.annotation.NonNull;

import com.aspire.android.russia.apiservices.RequestModel.BookRentalRequest;
import com.aspire.android.russia.apiservices.ResponseModel.BookRentalResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.aspire.android.russia.utils.StringUtil;

import org.json.JSONArray;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


public class WSBookingRental
        extends ApiProviderClient<BookRentalResponse> {

    private BookRentalRequest request;

    public WSBookingRental() {

    }

    public void booking(final BookRentalRequest request) {
        this.request = request;

    }

    @Override
    public void run(Callback<BookRentalResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        RequestBody partBookingId = createPartFromString(request.getBookingId() == null ?
                                                         "" :
                                                         request.getBookingId());
        RequestBody partUserID = createPartFromString(request.getUserID() == null ?
                                                      "" :
                                                      request.getUserID());
        RequestBody partPhone = createPartFromString(request.getPhone() == null ?
                                                     "" :
                                                     request.getPhone() + "");
        RequestBody partEmail = createPartFromString(request.getEmail() == null ?
                                                     "" :
                                                     request.getEmail() + "");
        RequestBody partBoth = createPartFromString(request.getBoth() == null ?
                                                    "" :
                                                    request.getBoth() + "");
        RequestBody partMobileNumber = createPartFromString(request.getMobileNumber() == null ?
                                                            "" :
                                                            request.getMobileNumber() + "");
        RequestBody partEmailAddress = createPartFromString(request.getEmailAddress() == null ?
                                                            "" :
                                                            request.getEmailAddress() + "");
        RequestBody partDriverName = createPartFromString(request.getDriverName() == null ?
                                                          "" :
                                                          request.getDriverName() + "");
        RequestBody partDriverAge = createPartFromString(request.getDriverAge() == null ?
                                                         "" :
                                                         request.getDriverAge() + "");
        RequestBody partInternationalLicense =
                createPartFromString(request.getInternationalLicense() == null ?
                                     "" :
                                     request.getInternationalLicense() + "");
        RequestBody partEpochPickupDate =
                createPartFromString(request.getEpochPickupDate() == null ?
                                     "" :
                                     request.getEpochPickupDate() + "");
        RequestBody partPickupTime = createPartFromString(request.getPickupTime() == null ?
                                                          "" :
                                                          request.getPickupTime() + "");
        RequestBody partPickupLocation = createPartFromString(request.getPickupLocation() == null ?
                                                              "" :
                                                              request.getPickupLocation() + "");
        RequestBody partEpochDropOffDate =
                createPartFromString(request.getEpochDropOffDate() == null ?
                                     "" :
                                     request.getEpochDropOffDate() + "");
        RequestBody partDropOffTime = createPartFromString(request.getDropOffTime() == null ?
                                                           "" :
                                                           request.getDropOffTime() + "");
        RequestBody partDropOffLocation =
                createPartFromString(request.getDropOffLocation() == null ?
                                     "" :
                                     request.getDropOffLocation() + "");

        JSONArray arrPreferredVehicles = new JSONArray(request.getPreferredVehicles());

        RequestBody partPreferredVehicles = createPartFromString(arrPreferredVehicles.toString());

        JSONArray arrPreferredCarRentals = new JSONArray(request.getPreferredCarRentals());
        RequestBody partPreferredCarRentals =
                createPartFromString(arrPreferredCarRentals.toString());
        RequestBody partMinimumPrice = createPartFromString(request.getMinimumPrice() == null ?
                                                            "" :
                                                            request.getMinimumPrice() + "");
        RequestBody partMaximumPrice = createPartFromString(request.getMaximumPrice() == null ?
                                                            "" :
                                                            request.getMaximumPrice() + "");
        RequestBody partSpecialRequirements =
                createPartFromString(request.getSpecialRequirements() == null ?
                                     "" :
                                     request.getSpecialRequirements() + "");

        MultipartBody.Part partUploadPhoto = null;
        if (request.getUploadPhoto() != null) {
            File file = new File(request.getUploadPhoto());
            if (!StringUtil.isEmpty(request.getUploadPhoto()) && file.exists()) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"),
                                           file);
                partUploadPhoto =
                        MultipartBody.Part.createFormData("picture",
                                                          file.getName(),
                                                          requestFile);
            } else {

            }
        }
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("BookingId", partBookingId);
        map.put("UserID", partUserID);
        map.put("Phone", partPhone);
        map.put("Email", partEmail);
        map.put("Both", partBoth);
        map.put("MobileNumber", partMobileNumber);
        map.put("EmailAddress", partEmailAddress);
        map.put("DriverName", partDriverName);
        map.put("DriverAge", partDriverAge);
        map.put("InternationalLicense", partInternationalLicense);
        map.put("EpochPickupDate", partEpochPickupDate);
        map.put("PickupTime", partPickupTime);
        map.put("PickupLocation", partPickupLocation);
        map.put("EpochDropOffDate", partEpochDropOffDate);
        map.put("DropOffTime", partDropOffTime);
        map.put("DropOffLocation", partDropOffLocation);
        RequestBody rb;
        for(int i=0;i<request.getPreferredVehicles().size();i++)
        {
            rb=RequestBody.create(MediaType.parse("text/plain"), request.getPreferredVehicles().get(i));
            map.put("referredVehicles["+i+"]", rb);
        }
        RequestBody rbRental;
        for(int i=0;i<request.getPreferredCarRentals().size();i++)
        {
            rbRental=RequestBody.create(MediaType.parse("text/plain"), request.getPreferredCarRentals().get(i));
            map.put("PreferredCarRentals["+i+"]", rbRental);
        }
        map.put("MinimumPrice", partMinimumPrice);
        map.put("MaximumPrice", partMaximumPrice);
        map.put("SpecialRequirements", partSpecialRequirements);


        serviceInterface.bookRental(map,partUploadPhoto                                   )
                        .enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent",
                    "Retrofit-Sample-App");
        return headers;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"),
                descriptionString);
    }


}
