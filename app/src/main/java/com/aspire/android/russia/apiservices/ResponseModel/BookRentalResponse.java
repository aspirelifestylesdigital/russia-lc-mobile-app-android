package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.BookingObject;


public class BookRentalResponse
        extends BaseResponse {
    private BookingObject Data;

    public BookingObject getData() {
        return Data;
    }

    public void setData(BookingObject data) {
        this.Data = data;
    }
}
