package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CRefreshTokenResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSRefreshToken
        extends B2CApiProviderClient<B2CRefreshTokenResponse>{

    public B2CWSRefreshToken(B2CICallback callback){
        this.b2CICallback = callback;
    }
    @Override
    public void run(Callback<B2CRefreshTokenResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.refreshToken(BuildConfig.B2C_CONSUMER_KEY,
                        SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""),
                        SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, "")).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CRefreshTokenResponse response) {
    }
}
