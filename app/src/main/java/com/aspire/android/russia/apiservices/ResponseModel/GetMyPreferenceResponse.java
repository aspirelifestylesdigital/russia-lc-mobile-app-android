package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.MyPreferencesObject;


public class GetMyPreferenceResponse
        extends BaseResponse {
    private MyPreferencesObject Data;

    public MyPreferencesObject getData() {
        return Data;
    }

    public void setData(MyPreferencesObject data) {
        this.Data = data;
    }
}
