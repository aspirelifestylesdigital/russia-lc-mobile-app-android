package com.aspire.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.apiservices.ResponseModel.SignUpResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.B2CWSGetUserDetails;
import com.aspire.android.russia.apiservices.b2c.B2CWSUpdateRegistration;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpdateRegistrationRequest;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.NetworkUtil;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.utils.StringUtil;
import com.aspire.android.russia.widgets.CustomErrorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aspire.android.russia.utils.CommonUtils.getCountryCode;

/**
 * Created by anh.trinh on 9/26/2016.
 */
public class MyProfileFragment
        extends BaseFragment
        implements SelectionFragment.SelectionCallback,
                   Callback {

    @BindView(R.id.tv_register)
    TextView mTvRegister;
    @BindView(R.id.tv_salutation)
    TextView mTvSalutation;
    @BindView(R.id.layout_salutation)
    RelativeLayout mLayoutSalutation;
    @BindView(R.id.tv_selected_salutation)
    TextView mTvSelectedSalutation;
    @BindView(R.id.salutation_error)
    CustomErrorView mSalutationError;
    @BindView(R.id.layout_selected_salutation)
    RelativeLayout mLayoutSelectedSalutation;
    @BindView(R.id.tv_mandatory)
    TextView mTvMandatory;
    @BindView(R.id.tv_title_first_name)
    TextView mTvTitleFirstName;
    @BindView(R.id.edt_first_name)
    EditText mEdtFirstName;
    @BindView(R.id.first_name_error)
    CustomErrorView mFirstNameError;
    @BindView(R.id.layout_first_name)
    RelativeLayout mLayoutFirstName;
    @BindView(R.id.tv_title_last_name)
    TextView mTvTitleLastName;
    @BindView(R.id.edt_last_name)
    EditText mEdtLastName;
    @BindView(R.id.last_name_error)
    CustomErrorView mLastNameError;
    @BindView(R.id.layout_last_name)
    RelativeLayout mLayoutLastName;
    @BindView(R.id.tv_mobile_no)
    TextView mTvMobileNo;
    @BindView(R.id.tv_country_no)
    TextView mTvCountryNo;
    @BindView(R.id.edt_phone_number)
    EditText mEdtPhoneNumber;
    @BindView(R.id.phone_error)
    CustomErrorView mPhoneError;
    @BindView(R.id.layout_mobile)
    RelativeLayout mLayoutMobile;
    @BindView(R.id.tv_we_will_need)
    TextView mTvWeWillNeed;
    @BindView(R.id.tv_title_zip_code)
    TextView mTvZipCode;
    @BindView(R.id.zip_code_error)
    CustomErrorView mZipCodeErrorView;
    @BindView(R.id.edt_zip_code)
    EditText mEdtZipCode;
    @BindView(R.id.tv_title_email)
    TextView mTvTitleEmail;
    @BindView(R.id.edt_email)
    EditText mEdtEmail;
    @BindView(R.id.layout_email)
    RelativeLayout mLayoutEmail;
    @BindView(R.id.tv_the_email_address)
    TextView mTvTheEmailAddress;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_submit)
    Button nBtnSubmit;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView nScrollView;
    String SalutationSelected = "";
    String CountryCodeSelected = "";
    String mUserID = "";


    @Override
    protected int layoutId() {
        return R.layout.fragment_my_profile;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(nBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                        "");
        if (!NetworkUtil.getNetworkStatus(getContext())) {
            showDialogMessage(getString(R.string.text_title_dialog_error),
                              getString(R.string.text_message_dialog_error_no_internet));
        } else {
            showDialogProgress();
            B2CWSGetUserDetails b2CWSGetUserDetails =
                    new B2CWSGetUserDetails(getUserDetailCallback);
            b2CWSGetUserDetails.run(null);
        }

        mEdtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                StringUtil.makeFirstLetterUpperCase(mEdtFirstName);
            }
        });
        mEdtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                StringUtil.makeFirstLetterUpperCase(mEdtLastName);
            }
        });
        mEdtEmail.setClickable(false);
        mEdtEmail.setFocusable(false);
    }

    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);

        return rootView;
    }

    @OnClick({R.id.tv_selected_salutation,
              R.id.tv_country_no,
              R.id.btn_cancel,
              R.id.btn_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_selected_salutation:
                mSalutationError.setVisibility(View.GONE);
                Bundle bundle = new Bundle();
                bundle.putString(SelectionFragment.PRE_SELECTION_TYPE,
                                 SelectionFragment.PRE_SELECTION_SALUTATION);
                bundle.putString(SelectionFragment.PRE_SELECTION_DATA,
                                 SalutationSelected);
                SelectionFragment fragment = new SelectionFragment();
                fragment.setSelectionCallBack(this);
                fragment.setArguments(bundle);
                pushFragment(fragment,
                             true,
                             true);
                break;
            case R.id.tv_country_no:
                mPhoneError.setVisibility(View.GONE);
                Bundle bundle1 = new Bundle();
                bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                                  SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
                bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                                  CountryCodeSelected);
                SelectionFragment fragment1 = new SelectionFragment();
                fragment1.setSelectionCallBack(this);
                fragment1.setArguments(bundle1);
                pushFragment(fragment1,
                             true,
                             true);

                break;
            case R.id.btn_cancel:
                onBackPress();
                break;
            case R.id.btn_submit:
                processSubmit();
                break;
        }
    }

    @OnTouch({R.id.edt_first_name,
              R.id.edt_last_name,
              R.id.edt_phone_number})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {

            case R.id.edt_first_name:
                mFirstNameError.setVisibility(View.GONE);
                break;
            case R.id.edt_last_name:
                mLastNameError.setVisibility(View.GONE);
                break;
            case R.id.edt_phone_number:
                mPhoneError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_SALUTATION)) {
            SalutationSelected = bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                                                  "");
            if(!TextUtils.isEmpty(SalutationSelected)) {
                mTvSelectedSalutation.setText(SalutationSelected);
                mTvSelectedSalutation.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }else{
                mTvSelectedSalutation.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
                mTvSelectedSalutation.setText(R.string.text_sign_up_please_select_salutation);
            }
        }
        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            CountryCodeSelected = bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                                                   "");
            if(!TextUtils.isEmpty(CountryCodeSelected)) {
                mTvCountryNo.setText(getCountryCode(CountryCodeSelected));
                mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }else{
                mTvCountryNo.setText(R.string.text_sign_up_cc);
                mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_my_profile));
        }
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof SignUpResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            SignUpResponse signUpResponse = ((SignUpResponse) response.body());
            int code = signUpResponse.getStatus();
            if (code == 200) {
                if (signUpResponse.getData() != null && signUpResponse.getData()
                                                                      .getUserItem() != null) {
                    setUserData(signUpResponse.getData()
                                              .getUserItem());


                }
            }
        }else{
            if (response.body() instanceof BaseResponse) {
                hideDialogProgress();
                if(getActivity()==null) {
                    return;
                }
                int code = ((BaseResponse)response.body()).getStatus();
                if (code == 200) {
                    onBackPress();
                }
            }
        }
    }

    private void setUserData(UserItem userData) {
        if(getActivity()==null) {
            return;
        }
        if(userData == null)
            return;
        if (!CommonUtils.stringIsEmpty(userData.getSalutation())) {
            SalutationSelected = userData.getSalutation();
            mTvSelectedSalutation.setText(userData.getSalutation());
        }
        if (!CommonUtils.stringIsEmpty(userData.getFirstName())) {
            mEdtFirstName.setText(userData.getFirstName());
        }
        if (!CommonUtils.stringIsEmpty(userData.getLastName())) {
            mEdtLastName.setText(userData.getLastName());
        }
        if (!CommonUtils.stringIsEmpty(userData.getEmail())) {
            mEdtEmail.setText(userData.getEmail());
        }
        if (!CommonUtils.stringIsEmpty(userData.getMobileNumber())) {
            fillCountryCodeFromPhone(userData.getMobileNumber());
        }
    }
    private void fillUserDataAtInit(){
        if(getActivity()==null) {
            return;
        }
        // Salutation
        SalutationSelected = SharedPreferencesUtils.getPreferences(AppConstant.USER_SALUTATION, "");
        if(CommonUtils.isStringValid(SalutationSelected)) {
            mTvSelectedSalutation.setText(SalutationSelected);
            mTvSelectedSalutation.setTextColor(getResources().getColor(R.color.text_color_active));
        }

        // First name
        mEdtFirstName.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_FIRST_NAME, ""));

        // Last name
        mEdtLastName.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_LAST_NAME, ""));

        // Zip code
        mEdtZipCode.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_ZIPCODE, ""));

        // Email
        mEdtEmail.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));

        // Mobile
        fillCountryCodeFromPhone(SharedPreferencesUtils.getPreferences(AppConstant.USER_MOBILE_NUMBER, ""));
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
    }

    private void fillCountryCodeFromPhone(String phone) {
        String[] resource = getResources().getStringArray(R.array.country_arrays);
        String countrCode = "";
        if(!phone.contains("+")){
            phone="+"+phone;
        }
        for (String s : resource) {
            if (phone.startsWith(getCountryCode(s))) {

                String countryNumber = getCountryCode(s);
                if(countryNumber.length()>countrCode.length()){
                    countrCode = getCountryCode(s);
                    CountryCodeSelected = s;
                    mTvCountryNo.setText(countryNumber);
                    mTvCountryNo.setTextColor(getResources().getColor(R.color.text_color_active));
                    mEdtPhoneNumber.setText(phone.substring(countryNumber.length()));
                }

            }
        }
    }

    private boolean validationSignUpInfo() {
        boolean isFieldRequired, isFirstNameInvalid, isLastNameInvalid, isMobileNumberInvalid, isZipCodeInvalid;
        isFieldRequired = isFirstNameInvalid = isLastNameInvalid = isMobileNumberInvalid = isZipCodeInvalid = false;
        if (SalutationSelected.equals("")) {
            mSalutationError.setVisibility(View.VISIBLE);
            mSalutationError.fillData(getString(R.string.text_sign_up_error_required_field));
            isFieldRequired = true;
        } else {
            mSalutationError.setVisibility(View.GONE);
        }
        if (mEdtFirstName.getText()
                         .toString()
                         .trim()
                         .equals("")) {
            mFirstNameError.setVisibility(View.VISIBLE);
            mFirstNameError.fillData(getString(R.string.text_sign_up_error_required_field));
            isFieldRequired = true;
        } else {
            mFirstNameError.setVisibility(View.GONE);
        }
        if (mEdtLastName.getText()
                        .toString()
                        .trim()
                        .equals("")) {
            mLastNameError.setVisibility(View.VISIBLE);
            mLastNameError.fillData(getString(R.string.text_sign_up_error_required_field));
            isFieldRequired = true;
        } else {
            mLastNameError.setVisibility(View.GONE);
        }

        if (mTvCountryNo.getText()
                        .toString()
                        .trim()
                        .equals(getString(R.string.text_sign_up_cc))) {
            mPhoneError.fillData(getString(R.string.text_sign_up_error_required_field));
            mPhoneError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else if (mEdtPhoneNumber.getText()
                .toString()
                .trim()
                .equals("")) {
            mPhoneError.fillData(getString(R.string.text_sign_up_error_required_field));
            mPhoneError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else {
            mPhoneError.setVisibility(View.GONE);
        }
        return !(isFieldRequired || isFirstNameInvalid || isLastNameInvalid || isMobileNumberInvalid || isZipCodeInvalid);
    }

    private void processSubmit() {
        if (validationSignUpInfo()) {
            if (!NetworkUtil.getNetworkStatus(getContext())) {
                showDialogMessage(getString(R.string.text_title_dialog_error),
                                  getString(R.string.text_message_dialog_error_no_internet));
            } else {
                showDialogProgress();
                B2CWSUpdateRegistration b2CWSUpdateRegistration =
                        new B2CWSUpdateRegistration(updateUserDetailCallback);
                b2CWSUpdateRegistration.setRegistrationRequest(new B2CUpdateRegistrationRequest().build(mEdtFirstName.getText()
                                                                                                                     .toString()
                                                                                                                     .trim(),
                                                                                                        mEdtLastName.getText()
                                                                                                                    .toString()
                                                                                                                    .trim(),
                                                                                                        mTvCountryNo.getText()
                                                                                                                    .toString()
                                                                                                                    .trim() + mEdtPhoneNumber.getText()
                                                                                                                                             .toString()
                                                                                                                                             .trim(),
                                                                                                        SalutationSelected,
                                                                                                        mEdtZipCode.getText()
                                                                                                                   .toString()));
                b2CWSUpdateRegistration.run(null);
            }

        }
    }
    private B2CICallback getUserDetailCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            if(response != null && response.isSuccess()){
                fillUserDataAtInit();
            }else{
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }
    };

    private B2CICallback updateUserDetailCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            if(response != null && response.isSuccess()){
                // Update first name and last name
                SharedPreferencesUtils.setPreferences(AppConstant.USER_FIRST_NAME, mEdtFirstName.getText().toString());
                SharedPreferencesUtils.setPreferences(AppConstant.USER_LAST_NAME, mEdtLastName.getText().toString());
                onBackPress();
            }else{
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }
    };

}
