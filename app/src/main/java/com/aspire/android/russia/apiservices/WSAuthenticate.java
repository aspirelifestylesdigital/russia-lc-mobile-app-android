package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.Map;

import retrofit2.Callback;


public class WSAuthenticate
        extends ApiProviderClient<AuthenticateResponse> {


    @Override
    public void run(Callback<AuthenticateResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        //serviceInterface.getHomeGourmet(request).enqueue(callback);
        authenticate(BuildConfig.AUTHEN_USERNAME, BuildConfig.AUTHEN_SECRET, callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }
}
