package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.CommonObject;

import java.util.List;


public class SmokingRoomPreferencesResponse
        extends BaseResponse {
    private List<CommonObject> Data;

    public List<CommonObject> getData() {
        return Data;
    }

    public void setData(List<CommonObject> data) {
        this.Data = data;
    }
}
