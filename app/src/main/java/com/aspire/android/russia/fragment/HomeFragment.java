package com.aspire.android.russia.fragment;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.adapter.HomeAdapter;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.ConciergeRequestObject;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.EntranceLock;
import com.aspire.android.russia.utils.Fontfaces;
import com.aspire.android.russia.utils.NetworkUtil;
import com.aspire.android.russia.utils.StringUtil;
import com.aspire.android.russia.widgets.CustomErrorView;

import org.greenrobot.eventbus.EventBus;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class HomeFragment extends BaseFragment implements HomeAdapter.OnHomePageClickListener
        , B2CICallback {

    @BindView(R.id.svHome)
    ScrollView svHome;
    @BindView(R.id.homeViewBorderLayout)
    View homeViewBorderLayout;
    @BindView(R.id.homeSearchLayout)
    View homeSearchLayout;

    @BindView(R.id.homeDiningItemLayout)
    View homeDiningItemLayout;
    @BindView(R.id.homeHotelItemLayout)
    View homeHotelItemLayout;
    @BindView(R.id.homeEntertainmentItemLayout)
    View homeEntertainmentItemLayout;
    @BindView(R.id.homeTransportationItemLayout)
    View homeTransportationItemLayout;
    @BindView(R.id.homeGolfItemLayout)
    View homeGolfItemLayout;
    @BindView(R.id.homeOtherItemLayout)
    View homeOtherItemLayout;

    @BindView(R.id.rlArrow)
    View rlArrow;
    @BindView(R.id.imgArrowDown)
    ImageView imgArrowDown;
    EntranceLock entranceLock = new EntranceLock();
    HomeSearchViewHolder homeSearchViewHolder;
    @Override
    protected int layoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        homeSearchViewHolder = new HomeSearchViewHolder(homeSearchLayout);
        homeViewBorderLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            boolean isFirstTime;
            @Override
            public void onGlobalLayout() {
                if(homeViewBorderLayout.getHeight() > 0 && !isFirstTime){
                    isFirstTime = true;
                    homeSearchLayout.getLayoutParams().height = homeViewBorderLayout.getHeight();
                    Log.d("ThuNguyen", "height = " + homeViewBorderLayout.getHeight());
                }
            }
        });

        // Init home concierge item
        initHomeConciergeItem(homeDiningItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Restaurant, getString(R.string.concierge_item_gourmet), R.drawable.img_concierge_service_gourmet));
        initHomeConciergeItem(homeHotelItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Hotel, getString(R.string.concierge_item_hotel), R.drawable.img_concierge_service_hotel));
        initHomeConciergeItem(homeEntertainmentItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Entertainment, getString(R.string.concierge_item_entertainment), R.drawable.img_concierge_service_entertainment));
        initHomeConciergeItem(homeTransportationItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Car, getString(R.string.concierge_item_car_rental), R.drawable.img_concierge_service_rental));
        initHomeConciergeItem(homeGolfItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Golf, getString(R.string.concierge_item_golf), R.drawable.img_concierge_service_golf));
        initHomeConciergeItem(homeOtherItemLayout, new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Other, getString(R.string.concierge_item_others), R.drawable.img_concierge_service_others));

        svHome.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = svHome.getScrollY(); // For ScrollView
               // Log.d("ThuNguyen", "ScrollY = " + scrollY);
                if(scrollY != 0){
                    setVisibilityForArrow(INVISIBLE);
                }else{
                    setVisibilityForArrow(VISIBLE);
            }
            }
        });
        rlArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onExpandClick(true);
            }
        });
        //makeArrowDance();
                        }
    public void makeArrowDance(){
        Animation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.4f);
        mAnimation.setDuration(500);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        imgArrowDown.setAnimation(mAnimation);
        imgArrowDown.invalidate();
                    }
    public void resetDance(){
        imgArrowDown.setAnimation(null);
                }
    public void setVisibilityForArrow(int visibility){
        rlArrow.setVisibility(visibility);
            }
    private void initHomeConciergeItem(View root, ConciergeRequestObject conciergeRequestObject){
        // Set onclick listener
        root.setClickable(true);
        root.setOnClickListener(onClickListener);

        TextView tvConciergeRequest = (TextView)root.findViewById(R.id.tvConciergeRequest);
        ImageView imgBackground = (ImageView)root.findViewById(R.id.imgBackground);
        tvConciergeRequest.setText(conciergeRequestObject.getBookingName());
        imgBackground.setImageResource(conciergeRequestObject.getImageRes());

    }
    private List<ConciergeRequestObject> createConciergeRequestObjectList(){
        List<ConciergeRequestObject> conciergeRequestObjectList = new ArrayList<>();
        conciergeRequestObjectList.add(null);
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Restaurant, getString(R.string.concierge_item_gourmet), R.drawable.img_concierge_service_gourmet));
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Hotel, getString(R.string.concierge_item_hotel), R.drawable.img_concierge_service_hotel));
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Entertainment, getString(R.string.concierge_item_entertainment), R.drawable.img_concierge_service_entertainment));
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Car, getString(R.string.concierge_item_car_rental), R.drawable.img_concierge_service_rental));
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Golf, getString(R.string.concierge_item_golf), R.drawable.img_concierge_service_golf));
        conciergeRequestObjectList.add(new ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE.Other, getString(R.string.concierge_item_others), R.drawable.img_concierge_service_others));
        return conciergeRequestObjectList;
    }
    @Override
    protected void bindData() {
        homeSearchViewHolder.updateWelcom();
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        showLogoApp(true);
        setSoftInputModeAdjustResize();
        updateWelcomText();
        resetPlacehoder();
        makeArrowDance();
        scrollToTop();
        ((HomeActivity)getActivity()).showFab(true);
        }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        resetDance();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
    }
    }

    @Override
    public void onExpandClick(boolean expanded) {
        svHome.smoothScrollBy(0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics()));
    }

    @Override
    public void onFavouriteClick(int position) {

    }

    @Override
    public void onChatbotSubmit(String text) {
        AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_HOME, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_CATEGORY_HOME);
        if(UserItem.isLogined()) {
            this.processBooking(text);
        } else {
            ((HomeActivity)getActivity()).showDialogRequiredSignIn();
        }
    }

    @Override
    public void showDialogNoConnection() {
        showDialogMessage(getString(R.string.text_title_dialog_error),getString(R.string.text_message_dialog_error_no_internet));
    }

    public void scrollToTop(){
        svHome.smoothScrollTo(0, 0);
    }
    public void updateWelcomText(){
        if(homeSearchViewHolder!=null){
            homeSearchViewHolder.updateWelcom();
        }
    }
    public void resetPlacehoder(){
        if(homeSearchViewHolder!=null){
            homeSearchViewHolder.resetPlaceHolder();
        }
    }
    private void processBooking(String text) {
        /*BookOtherRequest request = new BookOtherRequest();
        request.setWhatCanWeDo(text
                .toString()
                .trim());
        request.setPhone(true);
        request.setEmail(true);
        request.setBoth(true);
        request.setMobileNumber(UserItem.getLoginedMobileBumber());
        request.setEmailAddress(UserItem.getLoginedEmail());

        showDialogProgress();
        WSBookingOther wsBookingOther = new WSBookingOther(this);
        wsBookingOther.booking(request);
        wsBookingOther.run(this);*/
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.OTHER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.OTHER_REQUEST.getValue());

        upsertConciergeRequestRequest.updateDefaultPersonalInformation();
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails(text));

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(AppConstant.CONCIERGE_EDIT_TYPE.ADD, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }
    private String combineRequestDetails(String text){
        String requestDetails = AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + text;
        if(!TextUtils.isEmpty(UserItem.getLoginedFullName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(UserItem.getLoginedFullName());
        }
        return requestDetails;
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse) {
            if(response.isSuccess()) {
                //Reset Placeholder
                resetPlacehoder();
                scrollToTop();
                //goto thank you page
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                pushFragment(thankYouFragment,
                        true,
                        true);
            } else {
//                showToast(response.getMessage());
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(errorMessage);
        showDialogMessage("", getString(R.string.text_concierge_request_error));
    }
    public class HomeSearchViewHolder{
        @BindView(R.id.tvWelcome)
        public TextView tvWelcome;
        @BindView(R.id.tvHello)
        public TextView tvHello;
        @BindView(R.id.tvPlaceHolder)
        public TextView tvPlaceHolder;
        @BindView(R.id.etMessage)
        public EditText edtMessage;
        @BindView(R.id.etMessageError)
        public CustomErrorView etMessageError;
        @BindView(R.id.btnSubmit)
        public Button btnSubmit;
        @BindView(R.id.btnCancel)
        public Button btnCancel;

        public HomeSearchViewHolder(View view) {
            ButterKnife.bind(this, view);
            tvWelcome.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            //tvWelcomeName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            tvHello.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Light));
            btnSubmit.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
            btnCancel.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
            tvPlaceHolder.setTypeface(Fontfaces.getFont(getContext(),
                                                        Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            etMessageError.fillData(getContext().getString(R.string.text_sign_up_error_required_field));

            //edtMessage.setImeOptions(EditorInfo.IME_ACTION_DONE);
            //edtMessage.setImeActionLabel("AAAA", KeyEvent.KEYCODE_ENTER);

            updateWelcom();
            rotate(0);
            edtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(final View v,
                                          final boolean hasFocus) {
                    if(!hasFocus && edtMessage.getText().toString().trim().equals("")) {
                       resetPlacehoder();
                    }else{
                        edtMessage.setCursorVisible(true);
                        tvPlaceHolder.setAnimation(null);
                        tvPlaceHolder.setVisibility(View.GONE);
                        etMessageError.setVisibility(GONE);
                    }
                }
            });
        }
        public void rotate(int count) {

            //
            final int nextCount = count < 3 ?
                                  count + 1 :
                                  0;
            String[] strings = {getContext().getString(R.string.text_can_you_recommend),
                                getContext().getString(R.string.text_book_me_a_private_jet),
                                getContext().getString(R.string.text_book_a_private_chef),
                                getContext().getString(R.string.text_are_there_any_hidden)};
            tvPlaceHolder.setText(strings[count]);
            tvPlaceHolder.setAnimation(null);
            final Animation animationFadeIn = AnimationUtils.loadAnimation(getContext(),
                                                                           R.anim.fade_in);
            final Animation animationFadeOut = AnimationUtils.loadAnimation(getContext(),
                                                                            R.anim.fade_out);



            animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    new CountDownTimer(4000,
                                       1000) {

                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {
                            if(tvPlaceHolder.getVisibility()== View.VISIBLE) {
                                tvPlaceHolder.setAnimation(null);
                                tvPlaceHolder.startAnimation(animationFadeOut);
                            }
                        }
                    }.start();

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (nextCount >= 0) {
                        {
                            if (tvPlaceHolder.getVisibility() == View.VISIBLE) {
                                rotate(nextCount);
                            }
                        }
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeIn.setRepeatCount(0);
            animationFadeOut.setRepeatCount(0);
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.startAnimation(animationFadeIn);
        }
        public void resetPlaceHolder() {
            edtMessage.setText("");
            edtMessage.clearFocus();
            edtMessage.setCursorVisible(false);
            etMessageError.setVisibility(GONE);
            tvPlaceHolder.setVisibility(View.VISIBLE);
            CommonUtils.hideSoftKeyboard(getContext(), edtMessage);
            rotate(0);
        }
        public void updateWelcom(){
            if (UserItem.isLogined()) {
                //tvWelcomeName.setText(UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setText(getContext().getString(R.string.text_welcome) + " " + UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setVisibility(View.VISIBLE);
            } else {
                String wl = getContext().getString(R.string.text_welcome).replace(",", "")+getContext().getString(R.string.text_guest);
                tvWelcome.setText(wl);
                tvWelcome.setVisibility(View.VISIBLE);
            }
        }

        public void setData(){
            updateWelcom();
        }
        @OnClick(R.id.btnSubmit)
        void onSubmitClick() {

            if(UserItem.isLogined()) {
                String text = edtMessage.getText()
                                        .toString()
                                        .trim();
                if (StringUtil.isEmpty(text)) {
                    tvPlaceHolder.setAnimation(null);
                    etMessageError.setVisibility(View.VISIBLE);
                    tvPlaceHolder.setVisibility(GONE);


                    return;
                }
                if(!NetworkUtil.getNetworkStatus(getContext())){
                    showDialogNoConnection();
                    return;
                }
                if ( !entranceLock.isClickContinuous()) {
                    HomeFragment.this.onChatbotSubmit(text);
                }

               /* String text = edtMessage.getText().toString().trim();
                if (StringUtil.isEmpty(text)) {
                    tvPlaceHolder.setAnimation(null);
                    etMessageError.setVisibility(View.VISIBLE);
                    tvPlaceHolder.setVisibility(GONE);
                    return;
                }
                etMessageError.setVisibility(GONE);

                edtMessage.setText("");*/
            }else{
                ((HomeActivity)getContext()).showDialogRequiredSignIn();
            }
        }

        @OnClick(R.id.btnCancel)
        void onCancelClick() {
            resetPlaceHolder();
        }
       /* @OnTouch({R.id.etMessage})
        boolean onInputTouchMessage(final View v,
                                  final MotionEvent event) {
          *//*  edtMessage.setCursorVisible(true);
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.setVisibility(View.GONE);
            etMessageError.setVisibility(GONE);*//*

            return false;
        }*/

        public void onMenuClick(){

        }

        public void clearInputText(){
            edtMessage.setText("");
        }

    }
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        EntranceLock entranceLock = new EntranceLock();
        @Override
        public void onClick(View view) {
            if(!entranceLock.isClickContinuous()){
                if (UserItem.isLogined()) {
                    switch (view.getId()) {
                        case R.id.homeDiningItemLayout:
                            pushFragment(new ConciergeGourmetFragment(), true, true);
                            break;
                        case R.id.homeHotelItemLayout:
                            pushFragment(new BookingHotelFragment(), true, true);
                            break;
                        case R.id.homeEntertainmentItemLayout:
                            pushFragment(new BookingEventFragment(), true, true);
                            break;
                        case R.id.homeTransportationItemLayout:
                            pushFragment(new BookingCarRentalTransferFragment(), true, true);
                            break;
                        case R.id.homeGolfItemLayout:
                            pushFragment(new BookingGolfFragment(), true, true);
                            break;
                        case R.id.homeOtherItemLayout:
                            pushFragment(new BookingOtherFragment(), true, true);
                            break;
                    }
                }else{
                    ((HomeActivity)getActivity()).showDialogRequiredSignIn();
                }
            }
    }
    };
}
