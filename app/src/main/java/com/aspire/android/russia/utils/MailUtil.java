package com.aspire.android.russia.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.List;

/**
 * Created by nga.nguyent on 9/26/2016.
 */
public class MailUtil {
    public enum RESULT{
        SUCCESS("SUCCESS"),
        NO_EMAIL_SETUP("NOEMAILSETUP");

        private String message;

        RESULT(String message) {
            this.message = message;
        }
    }
    public static final String TYPE_EMAIL = "message/rfc822";

    private static MailUtil INSTANCE;
    private Context context;

    private MailUtil(Context context) {
        this.context = context;
    }

    public static MailUtil getInstance(Context context){
        if(INSTANCE==null){
            INSTANCE = new MailUtil(context);
        }
        return INSTANCE;
    }

    public RESULT sendEmail(String to, String title, String message){
        /* Create the Intent */
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /*
         * Fill it with Data
         * */
        //emailIntent.setType("plain/text");
        emailIntent.setType(TYPE_EMAIL);
        //TO: "to@email.com"
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{to});
        //"Subject"
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        //"Text"
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
        //input image, ect...
        //emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("uri://your/uri/string");

        /* Send it off to the Activity-Chooser */
        Intent chooser = Intent.createChooser(emailIntent, "Send mail...");

        // Verify the original intent will resolve to at least one activity
        if (emailIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(chooser);
            return RESULT.SUCCESS;
        }else {
            return RESULT.NO_EMAIL_SETUP;
        }
    }

    private ResolveInfo findGMail(){
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        //type of email
        intent.setType(TYPE_EMAIL);

        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        //
        ResolveInfo best = null;
        for (final ResolveInfo info : matches) {
            if (info.activityInfo.packageName.endsWith(".gm") ||
                    info.activityInfo.name.toLowerCase().contains("gmail")) {
                best = info;
                break;
            }
        }

        /*if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);*/

        return best;
    }

}
