package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.UserInfo;


public class SignUpResponse
        extends BaseResponse {
    private UserInfo Data;

    public UserInfo getData() {
        return Data;
    }

    public void setData(UserInfo data) {
        this.Data = data;
    }
}
