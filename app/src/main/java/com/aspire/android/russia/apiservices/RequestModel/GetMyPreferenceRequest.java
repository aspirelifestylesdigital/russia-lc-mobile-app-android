package com.aspire.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class GetMyPreferenceRequest
        extends BaseRequest {
    @Expose
    private String UserID;

    public String getMebershipCategory() {
        return MebershipCategory;
    }

    public void setMebershipCategory(final String mebershipCategory) {
        MebershipCategory = mebershipCategory;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    @Expose
    private String MebershipCategory;


}
