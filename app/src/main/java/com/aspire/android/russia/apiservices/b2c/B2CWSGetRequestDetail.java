package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CGetConciergeRequestDetailRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSGetRequestDetail
        extends B2CApiProviderClient<B2CGetRecentRequestResponse>{

    public B2CWSGetRequestDetail(B2CICallback callback){
        this.b2CICallback = callback;
    }
    private B2CGetConciergeRequestDetailRequest request;
    public void setValue(String epcCaseId, String transactionId){
        request = new B2CGetConciergeRequestDetailRequest().build(epcCaseId, transactionId);
    }
    @Override
    public void run(Callback<B2CGetRecentRequestResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        serviceInterface.getRequestDetail(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CGetRecentRequestResponse response) {
    }
}
