package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.RentalObject;

import java.util.List;


public class RentalResponse
        extends BaseResponse {
    private List<RentalObject> Data;

    public List<RentalObject> getData() {
        return Data;
    }

    public void setData(List<RentalObject> data) {
        this.Data = data;
    }
}
