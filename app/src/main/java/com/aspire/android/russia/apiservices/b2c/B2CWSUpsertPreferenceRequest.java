package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUpsertPreferenceResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSUpsertPreferenceRequest
        extends B2CApiProviderClient<B2CUpsertPreferenceResponse>{
    AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = AppConstant.PREFERENCE_EDIT_TYPE.ADD;
    private B2CUpsertPreferenceRequest request;

    public B2CWSUpsertPreferenceRequest(AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType, B2CICallback callback){
        this.b2CICallback = callback;
        this.preferenceEditType = preferenceEditType;
    }

    public void setRequest(B2CUpsertPreferenceRequest b2CUpsertRequest){
        request = b2CUpsertRequest;
    }
    @Override
    public void run(Callback<B2CUpsertPreferenceResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        switch (preferenceEditType){
            case ADD:
                serviceInterface.addPreferences(request).enqueue(this);
                break;
            case UPDATE:
                serviceInterface.updatePreferences(request).enqueue(this);
                break;
        }
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CUpsertPreferenceResponse response) {
    }
}
