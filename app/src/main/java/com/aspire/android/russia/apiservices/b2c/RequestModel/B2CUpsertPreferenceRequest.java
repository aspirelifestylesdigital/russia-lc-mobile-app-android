package com.aspire.android.russia.apiservices.b2c.RequestModel;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.RequestModel.BaseRequest;
import com.aspire.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CUpsertPreferenceRequest extends BaseRequest{
    @Expose
    private MemberExt Member;
    @Expose
    private List<PreferenceDetailRequest> PreferenceDetails;

    public B2CUpsertPreferenceRequest(){
        Member = new MemberExt();
    }
    public void setPreferenceDetails(PreferenceDetailRequest preferenceDetail){
        PreferenceDetails = new ArrayList<>();
        PreferenceDetails.add(preferenceDetail);
    }
    public class MemberExt{
        @Expose
        private String AccessToken;
        @Expose
        private String ConsumerKey;
        @Expose
        private String Functionality;
        @Expose
        private String OnlineMemberId;

        public MemberExt(){
            AccessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
            ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
            Functionality = "Preferences";
            OnlineMemberId = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
        }

        public void setAccessToken(String accessToken) {
            AccessToken = accessToken;
        }
    }

    public void setMember(MemberExt member) {
        Member = member;
    }

    public MemberExt getMember() {
        return Member;
    }
    public void setAccessToken(String accessToken){
        Member.AccessToken = accessToken;
    }
}
