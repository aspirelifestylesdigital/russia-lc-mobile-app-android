package com.aspire.android.russia.dcr;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.dcr.model.DCRPrivilegeObject;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfo;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfoLocation;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfoPrivileges;
import com.aspire.android.russia.fragment.ConciergeGourmetTab1Fragment;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;

import android.os.Bundle;
import android.view.View;

import butterknife.OnClick;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class PrivilegeDetailRestaurantFragment
        extends PrivilegeDetailBaseFragment {


    public PrivilegeDetailRestaurantFragment() {

    }

    @Override
    protected void fillDataCenter(DCRPrivilegeObject privilegeObject) {
        mDcrPrivilegeObject = privilegeObject;
        if (mDcrPrivilegeObject == null) {
            return;
        }
        boolean isShowLine = CommonUtils.isStringValid(privilegeObject.getSummary());
        if (CommonUtils.isStringValid(privilegeObject.getDescription())) {

            ItemPrivilegeDetailInfo itemDescription = new ItemPrivilegeDetailInfo(getContext());
            itemDescription.setData("",
                                    0,
                                    privilegeObject.getDescription(),
                                    0,
                                    privilegeObject.getTerms_and_conditions(),
                                    isShowLine,
                                    true);

            layoutPrivilegeInfo.addView(itemDescription);
        }else{
            if(CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions())) {

                ItemPrivilegeDetailInfo itemDescription = new ItemPrivilegeDetailInfo(getContext());
                itemDescription.setData("",
                                        0,
                                        "",
                                        0,
                                        privilegeObject.getTerms_and_conditions(),
                                        isShowLine,
                                        true);

                layoutPrivilegeInfo.addView(itemDescription);
            }
        }
        //PrivilegeDescription
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getDescription())|| CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions());
        }
        if (CommonUtils.isStringValid(privilegeObject.getStringPrivilegeDescription())) {

            ItemPrivilegeDetailInfoPrivileges itemPrivilegeDescription =
                    new ItemPrivilegeDetailInfoPrivileges(getContext());
            itemPrivilegeDescription.setPrivileges(privilegeObject.getStringPrivilegeDescription(),

                                                   "",
                                                   isShowLine);
            layoutPrivilegeInfo.addView(itemPrivilegeDescription);
        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getStringPrivilegeDescription());
        }
        if (CommonUtils.isStringValid(privilegeObject.getAddress())) {

            ItemPrivilegeDetailInfoLocation
                    itemLocation = new ItemPrivilegeDetailInfoLocation(getContext());
            itemLocation.setExpandAsDefault(false);
            itemLocation.setAddress(
                    privilegeObject.getAddress(),isShowLine);
            itemLocation.setPrivilegeDetailInfoListener(new ItemPrivilegeDetailInfoLocation.ItemPrivilegeDetailInfoListener() {
                @Override
                public void onTextInfoClick() {
                    if (mDcrPrivilegeObject == null) {
                        return;
                    }
                    openMap(mDcrPrivilegeObject.getLatitude(),
                            mDcrPrivilegeObject.getLongitude(),
                            mDcrPrivilegeObject.getAddress());
                }
            });
            layoutPrivilegeInfo.addView(itemLocation);

        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getAddress());
        }
        if (CommonUtils.isStringValid(privilegeObject.getStringCuisines())) {

            ItemPrivilegeDetailInfo itemCuisine = new ItemPrivilegeDetailInfo(getContext());
            itemCuisine.setData(getString(R.string.privileges_detail_title_cussine),
                                R.drawable.ic_cuisine,
                                privilegeObject.getStringCuisines(),
                                0,
                                "",isShowLine);
            layoutPrivilegeInfo.addView(itemCuisine);
        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getStringCuisines());
        }
        if (CommonUtils.isStringValid(privilegeObject.getStringOperatingHours())) {

            ItemPrivilegeDetailInfo itemOpenTime = new ItemPrivilegeDetailInfo(getContext());
            itemOpenTime.setData(getString(R.string.privileges_detail_title_operating_hours),
                                 R.drawable.ic_time_operation,
                                 privilegeObject.getStringOperatingHours(),
                                 0,
                                 "",isShowLine);
            layoutPrivilegeInfo.addView(itemOpenTime);
        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getStringOperatingHours());
        }
        if (CommonUtils.isStringValid(privilegeObject.getPricePoint())) {

            ItemPrivilegeDetailInfo itemPriceInfo = new ItemPrivilegeDetailInfo(getContext());
            itemPriceInfo.setData(getString(R.string.privileges_detail_title_price),
                                  R.drawable.ic_price,
                                  privilegeObject.getPricePoint(),
                                  0,
                                  "",isShowLine);
            layoutPrivilegeInfo.addView(itemPriceInfo);
        }
        tvVisitWebsite.setText(getString(R.string.privileges_detail_visit_restaurant_web_service));
    }

    @Override
    @OnClick({R.id.btnPrivilegeDetailBook})
    protected void onPrivilegeDetailBookClick(View view) {
        super.onPrivilegeDetailBookClick(view);
        if (UserItem.isLogined()) {

            BaseFragment fragment = new ConciergeGourmetTab1Fragment();
            Bundle bundle = new Bundle();
            if (mDcrPrivilegeObject != null) {
                bundle.putSerializable(AppConstant.PRE_RESTAURANT_DATA,
                                       mDcrPrivilegeObject);
            }
            fragment.setArguments(bundle);
            pushFragment(fragment,
                         true,
                         true);
        }
    }
}
