package com.aspire.android.russia.apiservices.b2c.RequestModel.preference;

import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailGolfRequest extends PreferenceDetailAddRequest{
    @Expose
    private String PreferredCourseName;
    @Expose
    private String PreferredTeeTimes;
    @Expose
    private String Value2; // Force to map API parameter :(

    public PreferenceDetailGolfRequest(String Delete) {
        super(Delete, AppConstant.PREFERENCE_TYPE.GOLF.getValue());
    }

    @Override
    public void fillData(Object... values) {
        List<String> valueList = StringUtil.removeAllSpecialCharactersAndFillInList(values);

        if(valueList != null){
            if(valueList.size() > 0){
                PreferredCourseName = valueList.get(0);
            }
            if(valueList.size() > 1){
                PreferredTeeTimes = valueList.get(1);
            }
            if(valueList.size() > 2){
                Value2 = valueList.get(2);
            }
        }
    }
}
