package com.aspire.android.russia.push;

import android.content.Context;
import android.content.Intent;

import com.google.firebase.FirebaseApp;

public class FCMUtils {
    /**
     * Call to the {@link FirebaseRegistrationIntentService} class which was taken from Google's
     * sample app for GCM integration
     */
    public static void handleGCMRegistration(Context ctx) {
        Intent intent = new Intent(ctx, FirebaseRegistrationIntentService.class);
        ctx.startService(intent);
    }
}
