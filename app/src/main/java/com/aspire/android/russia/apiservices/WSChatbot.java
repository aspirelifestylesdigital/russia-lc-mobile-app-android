package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.apiservices.ResponseModel.ChatbotResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.aspire.android.russia.utils.StringUtil;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSChatbot
        extends ApiProviderClient<ChatbotResponse> {

    String format = "json";
    String botid = "1";
    String convoid = "";
    String uid = "";
    String usname = "";
    String say = "";
    String countries = "";
    String cities = "";
    boolean location_request;
    String lati = "";
    String longi = "";
    String location = "";

    public WSChatbot(){

    }

    @Override
    public void run(Callback<ChatbotResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.chatBot(getFullUrl()).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        //headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

    private String getFullUrl(){
        String url = serviceInterface.WS_CHATBOT + "?";
        url += serviceInterface.WS_QUERY_FORMAT + "=" + format;
        url += "&" + serviceInterface.WS_QUERY_BOT_ID + "=" + botid;
        if(!StringUtil.isEmpty(convoid))
            url += "&" + serviceInterface.WS_QUERY_CONVO_ID + "=" + convoid;
        if(!StringUtil.isEmpty(uid))
            url += "&" + serviceInterface.WS_QUERY_UID + "=" + uid;
        if(!StringUtil.isEmpty(usname))
            url += "&" + serviceInterface.WS_QUERY_USNAME + "=" + usname;
        if(!StringUtil.isEmpty(say))
            url += "&" + serviceInterface.WS_QUERY_SAY + "=" + say;
        if(!StringUtil.isEmpty(countries))
            url += "&" + serviceInterface.WS_QUERY_COUNTRIES + "=" + countries;
        if(!StringUtil.isEmpty(cities))
            url += "&" + serviceInterface.WS_QUERY_CITIES + "=" + cities;
        url += "&" + serviceInterface.WS_QUERY_LOCATION_REQUEST + "=" + (location_request?1:0);
        if(!StringUtil.isEmpty(lati))
            url += "&" + serviceInterface.WS_QUERY_LAT + "=" + lati;
        if(!StringUtil.isEmpty(longi))
            url += "&" + serviceInterface.WS_QUERY_LONG + "=" + longi;
        if(!StringUtil.isEmpty(location))
            url += "&" + serviceInterface.WS_QUERY_LOCATION + "=" + location;
        return url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsname() {
        return usname;
    }

    public void setUsname(String usname) {
        this.usname = usname;
    }

    public String getSay() {
        return say;
    }

    public void setSay(String say) {
        this.say = say;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public boolean isLocation_request() {
        return location_request;
    }

    public void setLocation_request(boolean location_request) {
        this.location_request = location_request;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public String getConvoid() {
        return convoid;
    }

    public void setConvoid(String convoid) {
        this.convoid = convoid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
