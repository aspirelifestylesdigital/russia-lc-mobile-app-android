package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.google.gson.annotations.Expose;


public class GetListMyRequestResponse
        extends BaseResponse {
    @Expose
    public MyRequestResponseData Data;

    public MyRequestResponseData getData() {

        return Data;
    }

    public void setData(MyRequestResponseData data) {
        this.Data = data;
    }
}
