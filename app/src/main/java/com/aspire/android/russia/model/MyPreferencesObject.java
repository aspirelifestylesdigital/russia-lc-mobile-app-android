package com.aspire.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;


public class MyPreferencesObject
        implements Parcelable {
    public String getCarRentalsandTransfersOthers() {
        return CarRentalsandTransfersOthers;
    }

    public void setCarRentalsandTransfersOthers(final String carRentalsandTransfersOthers) {
        CarRentalsandTransfersOthers = carRentalsandTransfersOthers;
    }

    @Expose
    private String CarRentalsandTransfersOthers;
    @Expose
    private String UserID;
    @Expose
    private List<RentalObject> RentalVehicleTypes;
    @Expose
    private List<RentalObject> RentalCompanies;
    @Expose
    private List<KeyValueObject> CuisinePreferences;
    @Expose
    private String GourmetOthers;
    @Expose
    private List<KeyValueObject> GolfCourse;
    @Expose
    private String GolfGetawaysOthers;
    @Expose
    private List<KeyValueObject> TeeTimes;
    @Expose
    private String GolfCourseForApp;
    @Expose
    private String Others;

    @Expose
    private CommonObject RatingForApp;
    @Expose
    private List<CommonObject> RoomTypes;
    @Expose
    private List<CommonObject> Beds;
    @Expose
    private CommonObject SmookingRoom;
    @Expose
    private List<LoyalProgramObject> LoyalProgrammes;


    public List<LoyalProgramObject> getLoyalProgrammes() {
        return LoyalProgrammes;
    }

    public void setLoyalProgrammes(final List<LoyalProgramObject> loyalProgrammes) {
        LoyalProgrammes = loyalProgrammes;
    }

    public CommonObject getRatingForApp() {
        return RatingForApp;
    }

    public void setRatingForApp(final CommonObject ratingForApp) {
        RatingForApp = ratingForApp;
    }

    public List<CommonObject> getRoomTypes() {
        return RoomTypes;
    }

    public void setRoomTypes(final List<CommonObject> roomTypes) {
        RoomTypes = roomTypes;
    }

    public List<CommonObject> getBeds() {
        return Beds;
    }

    public void setBeds(final List<CommonObject> beds) {
        Beds = beds;
    }

    public CommonObject getSmookingRoom() {
        return SmookingRoom;
    }

    public void setSmookingRoom(final CommonObject smookingRoom) {
        SmookingRoom = smookingRoom;
    }



    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public List<RentalObject> getRentalVehicleTypes() {
        return RentalVehicleTypes;
    }

    public void setRentalVehicleTypes(final List<RentalObject> rentalVehicleTypes) {
        RentalVehicleTypes = rentalVehicleTypes;
    }

    public List<RentalObject> getRentalCompanies() {
        return RentalCompanies;
    }

    public void setRentalCompanies(final List<RentalObject> rentalCompanies) {
        RentalCompanies = rentalCompanies;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.UserID);
        parcel.writeList(this.RentalVehicleTypes);
        parcel.writeList(this.RentalCompanies);

    }
    protected MyPreferencesObject(Parcel in) {
        this.UserID = in.readString();
    }
    public static final Creator<MyPreferencesObject> CREATOR = new Creator<MyPreferencesObject>() {
        @Override
        public MyPreferencesObject createFromParcel(Parcel source) {
            return new MyPreferencesObject(source);
        }

        @Override
        public MyPreferencesObject[] newArray(int size) {
            return new MyPreferencesObject[size];
        }
    };

    public List<KeyValueObject> getCuisinePreferences() {
        return CuisinePreferences;
    }

    public void setCuisinePreferences(List<KeyValueObject> cuisinePreferences) {
        CuisinePreferences = cuisinePreferences;
    }

    public String getGourmetOthers() {
        return GourmetOthers;
    }

    public void setGourmetOthers(String gourmetOthers) {
        GourmetOthers = gourmetOthers;
    }

    public List<KeyValueObject> getGolfCourse() {
        return GolfCourse;
    }

    public void setGolfCourse(List<KeyValueObject> golfCourse) {
        GolfCourse = golfCourse;
    }

    public String getGolfGetawaysOthers() {
        return GolfGetawaysOthers;
    }

    public void setGolfGetawaysOthers(String golfGetawaysOthers) {
        GolfGetawaysOthers = golfGetawaysOthers;
    }

    public List<KeyValueObject> getTeeTimes() {
        return TeeTimes;
    }

    public void setTeeTimes(List<KeyValueObject> teeTimes) {
        TeeTimes = teeTimes;
    }

    public String getGolfCourseForApp() {
        return GolfCourseForApp;
    }

    public void setGolfCourseForApp(String golfCourseForApp) {
        GolfCourseForApp = golfCourseForApp;
    }

    public String getOthers() {
        return Others;
    }

    public void setOthers(String others) {
        Others = others;
    }
}