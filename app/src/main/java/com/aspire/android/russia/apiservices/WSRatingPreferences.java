package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.apiservices.ResponseModel.RatingPreferencesResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSRatingPreferences
        extends ApiProviderClient<RatingPreferencesResponse> {

    public WSRatingPreferences(){

    }
    @Override
    public void run(Callback<RatingPreferencesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getRatingPreferences().enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
