package com.aspire.android.russia.fragment;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SplashFourFragment
        extends BaseFragment {

    @Override
    protected int layoutId() {
        return R.layout.layout_splash_four;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void bindData() {
        /*if(getActivity() instanceof SplashAcitivy){
            ((SplashAcitivy)getActivity()).setInfo(getResources().getString(R.string.text_title_splash_four),getResources().getString(R.string.text_description_splash_four));
        }*/


    }

    @Override
    public boolean onBack() {
        return false;
    }


}
