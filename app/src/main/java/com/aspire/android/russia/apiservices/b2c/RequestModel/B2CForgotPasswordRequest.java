package com.aspire.android.russia.apiservices.b2c.RequestModel;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.RequestModel.BaseRequest;
import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CForgotPasswordRequest extends BaseRequest{
    @Expose
    private String ConsumerKey;
    @Expose
    private String Email2;
    @Expose
    private String Functionality;

    public B2CForgotPasswordRequest(){
        ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        Functionality = "ForgotPassword";
    }
    public B2CForgotPasswordRequest build(String email){
        Email2 = email;
        return this;
    }
}
