package com.aspire.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.apiservices.ResponseModel.BookGolfResponse;
import com.aspire.android.russia.apiservices.ResponseModel.GolfBookingDetailResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.aspire.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.aspire.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.concierge.GolfBookingDetailData;
import com.aspire.android.russia.model.concierge.MyRequestObject;
import com.aspire.android.russia.model.preference.GolfPreferenceDetailData;
import com.aspire.android.russia.model.preference.PreferenceData;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.DateTimeUtil;
import com.aspire.android.russia.utils.EntranceLock;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.NetworkUtil;
import com.aspire.android.russia.utils.PhotoPickerUtils;
import com.aspire.android.russia.utils.StringUtil;
import com.aspire.android.russia.widgets.AttachePhotoView;
import com.aspire.android.russia.widgets.ContactView;
import com.aspire.android.russia.widgets.CountryCityRequestView;
import com.aspire.android.russia.widgets.CustomErrorView;
import com.aspire.android.russia.widgets.NumberPickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookingGolfFragment
        extends BaseFragment implements SelectionFragment.SelectionCallback, SelectionCountryOrCityFragment.SelectionCallback, ContactView.ContactViewListener,
                                        Callback, B2CICallback {

    @BindView(R.id.edt_name_of_golf_course)
    EditText mEdtNameOfGolfCourse;
    @BindView(R.id.name_of_golf_course_error)
    CustomErrorView mNameOfGolfCourseError;
    @BindView(R.id.tv_date)
    TextView mTvDate;
    @BindView(R.id.tv_time)
    TextView mTvTime;
    @BindView(R.id.ervTime)
    CustomErrorView ervTime;
    @BindView(R.id.countryCityView)
    CountryCityRequestView countryCityRequestView;
    @BindView(R.id.picker_ballers)
    NumberPickerView ballerPickerView;
    @BindView(R.id.rb9Hole)
    RadioButton rb9Hole;
    @BindView(R.id.rb18Hole)
    RadioButton rb18Hole;
    @BindView(R.id.contact_view)
    ContactView mContactView;
    @BindView(R.id.tv_any_special_requirements_title)
    TextView mTvAnySpecialRequirementsTitle;
    @BindView(R.id.edt_any_special_requirements)
    EditText mEdtAnySpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;

    private Calendar pickCal;

    MyRequestObject myRequestObject;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isSubmitClicked = false;
    private GolfPreferenceDetailData golfPreferenceDetailData;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_golf;
    }


    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.makeFirstCharacterUpperCase(mEdtNameOfGolfCourse);

        ballerPickerView.setMinimum(1);
        pickCal = Calendar.getInstance();
        pickCal.add(Calendar.DAY_OF_YEAR, 1);
        pickCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA);
        showDateTime();
        ervTime.fillData(getString(R.string.text_error_message_invalid_time));
        ervTime.hide();
        specialReqError.setMessage(getString(R.string.special_requirement_error));

        mPhotoAttached.setParentFragment(this);
        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        mContactView.setContactViewListener(this);
        if (getArguments() != null) {
            myRequestObject = (MyRequestObject)getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        }else{ // Load default preferences
            showDialogProgress();
            B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
            b2CWSGetPreference.run(null);
        }
    }
    private void showDateTime(){
        mTvDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        mTvTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.TIME_FORMAT_CONCIERGE_BOOKING).toUpperCase());
    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }


    @OnClick({R.id.tv_date,
              R.id.tv_time,
              R.id.btn_submit,
              R.id.btn_cancel})
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (view.getId()) {
            case R.id.tv_date:
                ervTime.hide();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year,
                                                  int monthOfYear,
                                                  int dayOfMonth) {
                                if(CommonUtils.validateFutureDate(dayOfMonth, monthOfYear, year, view)) {
                                    pickCal.set(year, monthOfYear, dayOfMonth);
                                    showDateTime();
                                    if (DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                                        ervTime.setVisibility(View.GONE);
                                    } else {
                                        ervTime.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        },
                        pickCal.get(Calendar.YEAR),
                        pickCal.get(Calendar.MONTH),
                        pickCal.get(Calendar.DAY_OF_MONTH));
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
                datePickerDialog.show();
                break;
            case R.id.tv_time:
                ervTime.hide();
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        pickCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        pickCal.set(Calendar.MINUTE, minute);
                        showDateTime();
                        if(DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                            ervTime.setVisibility(View.GONE);
                        }else{
                            ervTime.setVisibility(View.VISIBLE);
                        }
                    }
                }, pickCal.get(Calendar.HOUR_OF_DAY), pickCal.get(Calendar.MINUTE), false);
                timePickerDialog.setTitle(getString(R.string.text_concierge_booking_golf_time).replace("*", ""));
                timePickerDialog.show();
                break;
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.GOLF.getValue());
                if(validationData() && !entranceLock.isClickContinuous()){
                    if(NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                    processBooking();
                    }else{
                        showInternetProblemDialog();
                    }
                }
                break;
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }
    @OnTouch({R.id.edt_name_of_golf_course,R.id.edt_any_special_requirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_name_of_golf_course:
                mNameOfGolfCourseError.setVisibility(View.GONE);
                break;
            case R.id.edt_any_special_requirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }
    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                          SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                          mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                     true,
                     true);
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA, ""));
        }


        /*if (typeSelection.equals(SelectionCountryOrCityFragment.PRE_SELECTION_CITY)) {
            mCitySelected =
                    bundle.getParcelable(SelectionCountryOrCityFragment.PRE_SELECTION_DATA);
            if (mCitySelected != null && CommonUtils.isStringValid(mCitySelected.getCityName())) {
                mTvPleaseSelectCity.setText(mCitySelected.getCityName());
            }
        }*/
    }
    private boolean validationData() {
        boolean result = true;
        if (!CommonUtils.isStringValid(mEdtNameOfGolfCourse.getText()
                                                    .toString()
                                                    .trim())) {
            mNameOfGolfCourseError.fillData(getString(R.string.text_sign_up_error_required_field));
            mNameOfGolfCourseError.setVisibility(View.VISIBLE);
            result = false;
        }
        if(!countryCityRequestView.isValidated()){
            result = false;
        }
        if(StringUtil.containSpecialCharacter(mEdtAnySpecialRequirements.getText().toString())){
            specialReqError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            specialReqError.setVisibility(View.GONE);
        }
        if(!DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)){
            result = false;
            ervTime.show();
        }else{
            ervTime.hide();
        }
        if(!mContactView.validationContact()){
            result = false;
        }


        return result;
    }
    private void processBooking() {
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.OTHER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.OTHER_REQUEST.getValue());
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);

        // Pickup date
        upsertConciergeRequestRequest.setStartDate(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
       /* upsertConciergeRequestRequest.setCity(edtCity.getText().toString());*/
         upsertConciergeRequestRequest.updateDataFromCountryCityView(countryCityRequestView);
        upsertConciergeRequestRequest.setNumberOfAdults(ballerPickerView.getNumber() + "");

        // Image path
        if(!TextUtils.isEmpty(uploadedPhotoPath)){
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        }else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos().size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(myRequestObject == null ? AppConstant.CONCIERGE_EDIT_TYPE.ADD : AppConstant.CONCIERGE_EDIT_TYPE.AMEND, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtAnySpecialRequirements.getText().toString().trim());
        }
        if(!TextUtils.isEmpty(mContactView.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_GUEST_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_DATE_OF_PLAY + DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS).substring(0,10);
        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_TEE_TIME + DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS).substring(11,19);

        if (!TextUtils.isEmpty(countryCityRequestView.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + countryCityRequestView.getCountry();
        }
        if (!TextUtils.isEmpty(countryCityRequestView.getState())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_STATE + countryCityRequestView.getState();
        }
        if(!TextUtils.isEmpty(countryCityRequestView.getCity())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_CITY+ countryCityRequestView.getCity();
        }
        if(!TextUtils.isEmpty(mEdtNameOfGolfCourse.getText().toString())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_GOLF_COURSE + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtNameOfGolfCourse.getText().toString());
        }
        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_NO_BALLERS + ballerPickerView.getNumber();

        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        if(rb9Hole.isChecked()){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_HOLE_PREFERENCE + rb9Hole.getText();
        }else{
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_HOLE_PREFERENCE + rb18Hole.getText();
        }
        if(!TextUtils.isEmpty(countryCityRequestView.getState())){
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_STATE + countryCityRequestView.getState();
        }
        if(golfPreferenceDetailData != null && !TextUtils.isEmpty(golfPreferenceDetailData.getPreferredGolfHandicap())){
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREFERRED_GOLF_HANDICAP + golfPreferenceDetailData.getPreferredGolfHandicap();
        }
        return requestDetails;
    }
    private void showBookingDetails(GolfBookingDetailData data){
        if(getActivity()==null) {
            return;
        }
       countryCityRequestView.setData(data);
        mEdtNameOfGolfCourse.setText(data.getConciergeRequestDetail().getGolfCourse());

        String holes = data.getConciergeRequestDetail().getHolepreference();
        if(!TextUtils.isEmpty(holes)){
            if(holes.equalsIgnoreCase(rb9Hole.getText().toString())){
                rb9Hole.setChecked(true);
            }else if(holes.equalsIgnoreCase(rb18Hole.getText().toString())){
                rb18Hole.setChecked(true);
            }
        }
        try {
        ballerPickerView.setNumber(Integer.parseInt(data.getConciergeRequestDetail().getNoballers()));
        }catch(Exception e){
            ballerPickerView.setNumber(0);
        }
        mEdtAnySpecialRequirements.setText(data.getConciergeRequestDetail().getSpecialRequirement());
        pickCal = Calendar.getInstance();
        pickCal.setTimeInMillis(data.getStartDateEpoch());
        showDateTime();

        // Contact view
        mContactView.updateContactView(data);
    }
    private void updateUIFromDefaultPreference(GolfPreferenceDetailData golfPreferenceDetailData){
        if(getActivity()==null) {
            return;
        }
        if(golfPreferenceDetailData != null){
            // Golf course name
            mEdtNameOfGolfCourse.setText(golfPreferenceDetailData.getCourseName());

            int teeTimeInHour = getHourFromTeeTime(golfPreferenceDetailData.getTeeTimes());
            if(teeTimeInHour > 0){
                pickCal.set(Calendar.MINUTE, 0);
                pickCal.set(Calendar.HOUR_OF_DAY, teeTimeInHour);
                showDateTime();
                if(DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)){
                    ervTime.show();
                }
            }
            /*Calendar teeTimeCal = Calendar.getInstance();
            teeTimeCal.setTimeInMillis(pickCal.getTimeInMillis());
            teeTimeCal.set(Calendar.MINUTE, 0);
            teeTimeCal.set(Calendar.HOUR_OF_DAY, teeTimeInHour);
            if(DateTimeUtil.shareInstance().checkLaterThan24Hours(teeTimeCal)){ // Tee time valid, show set it as default
                pickCal.setTimeInMillis(teeTimeCal.getTimeInMillis());
                showDateTime();
            }*/
        }
    }
    private int getHourFromTeeTime(String teeTime){
        if(!TextUtils.isEmpty(teeTime)){
            int colonIndex = teeTime.indexOf(":");
            if(colonIndex > 0){
                boolean isHourPM = teeTime.toLowerCase().contains("pm");
                int hour = Integer.parseInt(teeTime.substring(0, colonIndex));
                if(isHourPM){
                    if(hour != 12){
                        hour += 12;
                    }
                }
                return hour;
            }
        }
        return 0;
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse){
            if(response != null && response.isSuccess()) {
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.GOLF.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_BOOK.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            }else {
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
                if(upsertConciergeRequestRequest != null){
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }
        }else if(response instanceof B2CGetRecentRequestResponse){
            showBookingDetails(new GolfBookingDetailData((B2CGetRecentRequestResponse)response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if(upsertConciergeRequestRequest != null){
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }
    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookGolfResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            BookGolfResponse bookGolfResponse = ((BookGolfResponse) response.body());
            int code = bookGolfResponse.getStatus();
            if (code == 200) {
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ThankYouFragment.LINE_MAIN,
                            getString(R.string.text_message_thank_you_amend));
                    thankYouFragment.setArguments(bundle);
                }
                pushFragment(thankYouFragment,
                             true,
                             true);

            }else{
//                showToast(bookGolfResponse.getMessage());
            }
        } else if(response.body() instanceof GolfBookingDetailResponse){
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            showBookingDetails(((GolfBookingDetailResponse) response.body()).getData());
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode== PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode== PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE){
                mPhotoAttached.onActivityResult(requestCode, data);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_concierge_booking_golf_title));
        }
    }
    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity() != null) {
            hideDialogProgress();
                if(getActivity()==null) {
                    return;
                }
                golfPreferenceDetailData = (GolfPreferenceDetailData) PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.GOLF);
            updateUIFromDefaultPreference(golfPreferenceDetailData);
        }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }
    };
}
