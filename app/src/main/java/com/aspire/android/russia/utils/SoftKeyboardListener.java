package com.aspire.android.russia.utils;

import android.graphics.Rect;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

/**
 * Created by nganguyent on 29/07/2016.
 * Note: This only works when android:windowSoftInputMode of your activity is set to adjustResize in the manifest.
 */
public class SoftKeyboardListener {

    public interface OnSoftKeyboardListener{
        void onKeyboardShow(int height);
        void onKeyboardHide(int height);
    }

    //private Activity activity;
    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;
    private OnSoftKeyboardListener onSoftKeyboardListener;

    /**
     * Call this method ưhen initial fragment or activity
     * @param rootView main view in your layout
     * */
    public void attachKeyboardListeners(/*Activity activity,*/ ViewGroup rootView, OnSoftKeyboardListener listener) {

        if (keyboardListenersAttached) {
            System.out.println("Keyboard has attached.");
            return;
        }

        //this.activity = activity;
        this.rootLayout = rootView;
        this.onSoftKeyboardListener = listener;

        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
        keyboardListenersAttached = true;

    }

    public void detachKeyboardListener(){
        if(rootLayout==null){
            if(keyboardListenersAttached)
                keyboardListenersAttached = false;
            return;
        }

        if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
            keyboardListenersAttached = false;
        }

        //activity = null;
        rootLayout = null;
        onSoftKeyboardListener = null;
    }

    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            /*int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
            int contentViewTop = activity.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);

            if(heightDiff <= contentViewTop){
                onHideKeyboard();

                *//*Intent intent = new Intent("KeyboardWillHide");
                broadcastManager.sendBroadcast(intent);*//*

                System.out.println("KeyboardWillHide");

            } else {
                int keyboardHeight = heightDiff - contentViewTop;
                onShowKeyboard(keyboardHeight);

                *//*Intent intent = new Intent("KeyboardWillShow");
                intent.putExtra("KeyboardHeight", keyboardHeight);

                broadcastManager.sendBroadcast(intent);*//*

                System.out.println("KeyboardWillShow: " + keyboardHeight);
            }*/
            if(rootLayout==null)
                return;

            Rect r = new Rect();
            rootLayout.getWindowVisibleDisplayFrame(r);
            int screenHeight = rootLayout.getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            //Log.d(TAG, "keypadHeight = " + keypadHeight);

            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                if(onSoftKeyboardListener!=null){
                    onSoftKeyboardListener.onKeyboardShow(keypadHeight);
                }
                else {
                    System.out.println("Keyboard Show: " + keypadHeight);
                }
            }
            else {
                // keyboard is closed
                if(onSoftKeyboardListener!=null){
                    onSoftKeyboardListener.onKeyboardHide(keypadHeight);
                }
                else {
                    System.out.println("Keyboard Hide: " + keypadHeight);
                }
            }

        }
    };

}