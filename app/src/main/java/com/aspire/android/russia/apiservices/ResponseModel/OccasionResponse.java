package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.KeyValueObject;
import com.google.gson.annotations.Expose;

import java.util.List;


public class OccasionResponse
        extends BaseResponse {
    @Expose
    List<KeyValueObject> Data;

    public List<KeyValueObject> getData() {
        return Data;
    }

    public void setData(List<KeyValueObject> data) {
        Data = data;
    }
}
