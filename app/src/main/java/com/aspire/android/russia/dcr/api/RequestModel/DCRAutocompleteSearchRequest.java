package com.aspire.android.russia.dcr.api.RequestModel;

import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 4/5/2017.
 */

public class DCRAutocompleteSearchRequest extends DCRBaseRequest {

    @Expose
    private String term;
    @Expose
    private Boolean filter_sold_out;
    @Expose
    private Integer filter_min_photo_quantity;
    @Expose
    private String item_type;
    @Expose
    private Integer limit;

    public DCRAutocompleteSearchRequest(){
    }

    public DCRAutocompleteSearchRequest setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getTerm() {
        return term;
    }

    public DCRAutocompleteSearchRequest setTerm(String term) {
        this.term = term;
        return this;
    }

    public boolean isFilter_sold_out() {
        return filter_sold_out;
    }

    public DCRAutocompleteSearchRequest setFilter_sold_out(boolean filter_sold_out) {
        this.filter_sold_out = filter_sold_out;
        return this;
    }

    public int getFilter_min_photo_quantity() {
        return filter_min_photo_quantity;
    }

    public DCRAutocompleteSearchRequest setFilter_min_photo_quantity(int filter_min_photo_quantity) {
        this.filter_min_photo_quantity = filter_min_photo_quantity;
        return this;
    }

    public String getItem_type() {
        return item_type;
    }

    public DCRAutocompleteSearchRequest setItem_type(String item_type) {
        this.item_type = item_type;
        return this;
    }

    public int getLimit() {
        return limit;
    }

    public DCRAutocompleteSearchRequest setLimit(int limit) {
        this.limit = limit;
        return this;
    }
}
