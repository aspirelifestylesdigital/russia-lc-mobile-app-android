package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.FavouriteData;
import com.google.gson.annotations.Expose;


public class AddWishListResponse
        extends BaseResponse {
    @Expose
    FavouriteData Data;

    public FavouriteData getData() {
        return Data;
    }

    public void setData(FavouriteData data) {
        Data = data;
    }
}
