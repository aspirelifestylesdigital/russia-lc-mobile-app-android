package com.aspire.android.russia.apiservices.b2c;

import android.text.TextUtils;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUploadFileResponse;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Callback;

public class B2CWSUpsertConciergeRequest
        extends B2CApiProviderClient<B2CUpsertConciergeRequestResponse>{
    AppConstant.CONCIERGE_EDIT_TYPE conciergeEditType = AppConstant.CONCIERGE_EDIT_TYPE.ADD;
    private B2CUpsertConciergeRequestRequest request;
    private B2CWSUploadMediaFile b2CWSUploadMediaFile;

    public B2CWSUpsertConciergeRequest(AppConstant.CONCIERGE_EDIT_TYPE conciergeEditType, B2CICallback callback){
        this.b2CICallback = callback;
        this.conciergeEditType = conciergeEditType;
    }

    public void setRequest(B2CUpsertConciergeRequestRequest b2CUpsertRequest){
        request = b2CUpsertRequest;
    }
    @Override
    public void run(Callback<B2CUpsertConciergeRequestResponse> cb) {
        this.callback = cb; // Upload photo first if any
        if(!TextUtils.isEmpty(request.getAttachmentPath()) && !request.getAttachmentPath().startsWith("~/")){
            b2CWSUploadMediaFile = new B2CWSUploadMediaFile(uploadPhotoCallback);
            b2CWSUploadMediaFile.setLocalPath(request.getAttachmentPath());
            b2CWSUploadMediaFile.run(null);
        }else {
            super.checkAuthenticateAndRun();
        }
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        switch (conciergeEditType){
            case ADD:
                request.setEditType(AppConstant.CONCIERGE_EDIT_TYPE.ADD.getValue());
                serviceInterface.createConciergeRequest(request).enqueue(this);
                break;
            case AMEND:
                request.setEditType(AppConstant.CONCIERGE_EDIT_TYPE.AMEND.getValue());
                serviceInterface.updateConciergeRequest(request).enqueue(this);
                break;
            case CANCEL:
                request.setEditType(AppConstant.CONCIERGE_EDIT_TYPE.CANCEL.getValue());
                serviceInterface.updateConciergeRequest(request).enqueue(this);
                break;
        }
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CUpsertConciergeRequestResponse response) {
    }

    private B2CICallback uploadPhotoCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if(response != null){
                if(response.isSuccess()){
                    B2CUploadFileResponse b2CUploadFileResponse = (B2CUploadFileResponse)response;
                    if(b2CUploadFileResponse.getUploadedFiles() != null && b2CUploadFileResponse.getUploadedFiles().size() > 0) {
                        request.setAttachmentPath(b2CUploadFileResponse.getUploadedFiles().get(0));
                    }
                    checkAuthenticateAndRun();
                }else{
                    b2CICallback.onB2CFailure(response.getMessage(), response.getErrorCode());
                }
            }else{
                b2CICallback.onB2CFailure(b2CWSUploadMediaFile.getMessageErrorFromLog(), b2CWSUploadMediaFile.getErrorCodeFromLog());
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            b2CICallback.onB2CFailure(errorMessage, errorCode);
        }
    };
}
