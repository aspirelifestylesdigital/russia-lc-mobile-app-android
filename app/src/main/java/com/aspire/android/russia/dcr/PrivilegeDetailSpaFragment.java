package com.aspire.android.russia.dcr;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.dcr.model.DCRPrivilegeObject;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfo;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfoLocation;
import com.aspire.android.russia.dcr.widgets.ItemPrivilegeDetailInfoPrivileges;
import com.aspire.android.russia.fragment.BookingOtherFragment;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;

import android.os.Bundle;
import android.view.View;

import butterknife.OnClick;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class PrivilegeDetailSpaFragment
        extends PrivilegeDetailBaseFragment {


    public PrivilegeDetailSpaFragment() {

    }

    @Override
    protected void fillDataCenter(DCRPrivilegeObject privilegeObject) {

        mDcrPrivilegeObject = privilegeObject;
        if (mDcrPrivilegeObject == null) {
            return;
        }

        tvSummary.setVisibility(View.GONE);
        // Display Summary
        //boolean isShowLine = CommonUtils.isStringValid(privilegeObject.getSummary());
        boolean isShowLine = false;
        if (CommonUtils.isStringValid(privilegeObject.getDescription())) {

            ItemPrivilegeDetailInfo itemDescription = new ItemPrivilegeDetailInfo(getContext());
            itemDescription.setData("",
                                    0,
                                    privilegeObject.getDescription(),
                                    0,
                                    privilegeObject.getTerms_and_conditions(),
                                    isShowLine,
                                    true);

            layoutPrivilegeInfo.addView(itemDescription);
        }else{
            if(CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions())) {

                ItemPrivilegeDetailInfo itemDescription = new ItemPrivilegeDetailInfo(getContext());
                itemDescription.setData("",
                                        0,
                                        "",
                                        0,
                                        privilegeObject.getTerms_and_conditions(),
                                        isShowLine,
                                        true);

                layoutPrivilegeInfo.addView(itemDescription);
            }
        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getDescription())|| CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions());
        }

        //Privilege Description
        String privileges = privilegeObject.getStringPrivilegeDescription();
        if ( CommonUtils.isStringValid(privileges)) {
            ItemPrivilegeDetailInfoPrivileges itemPrivilegeDescription =
                    new ItemPrivilegeDetailInfoPrivileges(getContext());
            itemPrivilegeDescription.setExpandAsDefault(true);
            itemPrivilegeDescription.setPrivileges(privileges,
                    privilegeObject.getTerms_and_conditions(),
                    isShowLine);

            layoutPrivilegeInfo.addView(itemPrivilegeDescription);

        }else{
            if(CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions())) {

                ItemPrivilegeDetailInfo itemPrivilegeDescription =
                        new ItemPrivilegeDetailInfo(getContext());
                itemPrivilegeDescription.setData("",
                        0,
                        "",
                        0,
                        privilegeObject.getTerms_and_conditions(),
                        isShowLine,
                        true);
                layoutPrivilegeInfo.addView(itemPrivilegeDescription);
            }
        }

        // Display Location
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privileges);
        }
        if (CommonUtils.isStringValid(privilegeObject.getAddress())) {

            ItemPrivilegeDetailInfoLocation
                    itemLocation = new ItemPrivilegeDetailInfoLocation(getContext());
            itemLocation.setExpandAsDefault(false);
            itemLocation.setAddress(
                    privilegeObject.getAddress(),isShowLine);
            itemLocation.setPrivilegeDetailInfoListener(new ItemPrivilegeDetailInfoLocation.ItemPrivilegeDetailInfoListener() {
                @Override
                public void onTextInfoClick() {
                    if (mDcrPrivilegeObject == null) {
                        return;
                    }
                    openMap(mDcrPrivilegeObject.getLatitude(),
                            mDcrPrivilegeObject.getLongitude(),
                            mDcrPrivilegeObject.getAddress());
                }
            });
            layoutPrivilegeInfo.addView(itemLocation);

        }

        // Display Hours
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getAddress());
        }
        if (CommonUtils.isStringValid(privilegeObject.getStringOperatingHours())) {

            ItemPrivilegeDetailInfo itemOpenTime = new ItemPrivilegeDetailInfo(getContext());
            itemOpenTime.setData(getString(R.string.privileges_detail_title_operating_hours),
                                 R.drawable.ic_time_operation,
                                 privilegeObject.getStringOperatingHours(),
                                 0,
                                 "",isShowLine);
            layoutPrivilegeInfo.addView(itemOpenTime);
        }

        /*
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getStringOperatingHours());
        }
        if (CommonUtils.isStringValid(privilegeObject.getTreatmeants())) {

            ItemPrivilegeDetailInfo itemOpenTime = new ItemPrivilegeDetailInfo(getContext());
            itemOpenTime.setData(getString(R.string.privileges_detail_title_treatments),
                                 R.drawable.ic_info_active,
                                 privilegeObject.getTreatmeants(),
                                 0,
                                 "",isShowLine);
            layoutPrivilegeInfo.addView(itemOpenTime);
        }*/

        tvVisitWebsite.setText(getString(R.string.privileges_detail_visit_spa_web_service));
    }

    @Override
    @OnClick({R.id.btnPrivilegeDetailBook})
    protected void onPrivilegeDetailBookClick(View view) {
        super.onPrivilegeDetailBookClick(view);
        if (UserItem.isLogined()) {
            BaseFragment fragment = new BookingOtherFragment();
            Bundle bundle = new Bundle();
            if (mDcrPrivilegeObject != null) {
                bundle.putSerializable(AppConstant.PRE_SPA_DATA,
                                       mDcrPrivilegeObject);
            }
            fragment.setArguments(bundle);
            pushFragment(fragment,
                         true,
                         true);

        }
    }
}
